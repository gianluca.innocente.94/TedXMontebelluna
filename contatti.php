

<section class="section contact-form no-bottom-line">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3>Scrivici un <span class="alternate"><b>Messaggio</b></span></h3>
				</div>
			</div>
		</div>
		<form action="#" class="row" id="contact_form">
			<div class="col-md-6">
				<input type="text" class="form-control main" name="name" id="name" placeholder="Nome">
			</div>
			<div class="col-md-6">
				<input type="email" class="form-control main" name="email" id="email" placeholder="Email">
			</div>
			<div class="col-md-12">
				<input type="text" class="form-control main" name="phone" id="phone" placeholder="Telefono">
			</div>
			<div class="col-md-12">
				<textarea name="message" id="message" class="form-control main" rows="10" placeholder="Messaggio"></textarea>
			</div>
			<div class="col-sm-12 text-center">
                <div align="center">
                    <div class="g-recaptcha" data-sitekey="6LfWq1EUAAAAAHHeSpddiTlCJmMzStQ9uBfqJz7a"></div>
                </div>
            </div>
			<div class="col-12 text-center">
				<button type="submit" class="btn btn-main-md">Invia</button>
			</div>
			<div class="col-12 text-center">
				<div id="contact_message" class="form-message"></div>
			</div>
		</form>
	</div>
</section>
<script>
	$(document).ready(function() {
        $("#contact_form").submit(function() {
            $("#contact_message").hide();

            var params = {
                name: $("#name").val(),
                email: $("#email").val(),
                phone: $("#phone").val(),
                msg: $("#message").val(),
                captcha_response: $("#g-recaptcha-response").val()
            };

            $.post("core/contact.php", params, function (data) {
                $("#contact_message").html(data);
                $("#contact_message").fadeIn();

                return false;
            }).fail(function (error) {
                $("#contact_message").html("Si è verificato un errore imprevisto. Ti preghiamo di contattarci scrivendo a info@tedxmontebelluna.com");

                $("#contact_message").fadeIn();
            });

            return false;
        });

        $("#teaser_modal").on('hidden.bs.modal', function (e) {
            $("#teaser_modal iframe").attr("src", $("#teaser_modal iframe").attr("src"));
        });
	});
</script>