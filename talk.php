<style>
hr {
    border-top: 15px solid #ff2b06;
    margin: 0;
}
</style>
<section class="section contact-form no-bottom-line">
<br />
<div align="center"><h1 class="talks-title">TEDxMontebelluna <span style="color: #ff2b06; font-weight: bold;">Buon Vento</span></h1></div>
<br /><br />

<hr />
<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/DURANTE.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Daniele Durante <span>La geometria delle relazioni</span></h2>
                        <p>Daniele Durante è un giovane Professore di Statistica presso il Dipartimento di Scienze delle Decisioni dell’Università Bocconi di Milano. Ha studiato Scienze Statistiche tra l’Università di Padova e la Duke University negli Stati Uniti. <br />Si definisce uno “scienziato dei dati” e ama la natura, soprattutto quando si esprime tramite numeri e linguaggi matematici. <br /><br />A TEDx Montebelluna ci ha spiegato la geometria nascosta delle relazioni che instauriamo ogni giorno, proiettandoci in nuovi spazi matematici dove le sequenze di connessioni tra persone che ci dividono influenzano la nostra vita e le nostre decisioni ben oltre le distanze fisiche.</p>
                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/8uJPTzj_ilA?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe src="https://www.youtube.com/embed/mcHz_ycaC6Q?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen height="300" width="70%"></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>

<hr />
<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/WEYLAND.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                <h2>Beate Weyland <span>Progettare scuole insieme tra pedagogia, architettura e design</span></h2>
                <p>Beate Weyland è professore associato di didattica presso la Facoltà di Scienze della Formazione della Libera Università di Bolzano. <br />La sua ricerca approfondisce il rapporto tra pedagogia, architettura e design nella trasformazione e sviluppo della scuola, ed esplora la qualità della progettazione condivisa. <br />Opera all’interno del collettivo PULS e PAD, un gruppo di pedagogisti, architetti e designer.  Il collettivo promuove studi, mostre e convegni a livello internazionale per sensibilizzare sul rapporto tra spazi e didattiche e per dare informazioni pedagogiche al corpo materiale della scuola.<br />Nella sua talk “Progettare scuole insieme tra pedagogia, architettura e design” ci ha parlato degli spazi scolastici che sono, da una parte, specchi di un modo di pensare e lavorare, e dall’altra potenti dispositivi pedagogici. Ci ha mostrato una possibile via per progettare i luoghi della cultura del sapere come spazi di benessere per tutti.</p>

                <div class="row video-row">
                    <div class="col-sm-6">
                        <h3>La presentazione di Franco e Giulio</h3>
                        <div class="videoWrapper">
                            <div class="videoWrapperAspect">
                                <iframe width="70%" height="300" src="https://www.youtube.com/embed/LdHlXMJNbk0?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <h3>La talk</h3>
                        <div class="videoWrapper">
                            <div class="videoWrapperAspect">
                                <iframe width="70%" height="300" src="https://www.youtube.com/embed/fReZNnRtPSE?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/MARCOLIN.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Luca Marcolin <span>Cosa ho imparato dalle famiglie imprenditoriali per vivere insieme nella nostra società</span></h2>
                        <p>Luca Marcolin è counselor, coach e trainer di programmazione neuro-linguistica.<br />Dopo la Laurea in Economia Aziendale si è occupato di risorse umane, di organizzazione e controllo di gestione per grandi aziende.<br />Ha poi sviluppato la sua società di consulenza, The Family Business Unit, e si è dedicato sempre di più alle imprese di famiglia, accompagnando gli imprenditori e il management nella loro crescita.<br />Tiene lezioni di management e di family business presso l'Università di Padova e la Business School CUOA.<br />A TEDx Montebelluna ci ha parlato di imprese familiari e della loro sfida nel combinare armonia e risultati. Ci ha raccontato cosa ha imparato dalle famiglie imprenditoriali che può essere utile per vivere le nostre famiglie e per gestire le nostre imprese.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/UF-vVR6nhgU?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/YRLUziRfgCs?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/BELLAVISTA.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Massimo Soriani Bellavista <span>Per costruire bisogna prima rompere... gli schemi</span></h2>
                        <p>Massimo Soriani Bellavista collabora da vent’anni con Edward De Bono, l’inventore del pensiero laterale di cui è Master Trainer e rappresentante ufficiale in Italia.<br />Laureato in psicologia del lavoro, negli anni ha integrato vari modelli di business tra cui una metodologia chiamata Markethink che integra pensiero laterale, brainstorming e marketing. <br />Da dodici anni collabora con la scuola Universitaria Supsi in Svizzera per le aree di management, risorse umane e creatività. Ha fondato due società di formazione e consulenza che si occupano di formazione aziendale e digital innovation. <br />Nel suo speech a TEDx Montebelluna ci ha portati in un viaggio nel pensiero laterale e ci ha dato degli stimoli su come canalizzare le buone idee attraverso la rete e trasformarle in progetti creativi.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/7vCH1H88Neg?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/SFCQT0EuPUA?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/GHENO.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Vera Gheno <span>Il potere delle parole giuste</span></h2>
                        <p>Vera Gheno è una sociolinguista e si occupa da vent'anni di comunicazione mediata dal computer. Insegna all'università di Firenze e tiene corsi sull'etica della comunicazione in scuole, aziende e master universitari.<br />Gestisce il profilo Twitter dell'Accademia della Crusca e collabora con Zanichelli per questioni di lingua e di educazione ai nuovi media. Traduce letteratura dall’ungherese all’italiano. <br />È autrice di due libri: “Guida pratica all’italiano scritto (senza diventare grammarnazi)”e “Social-linguistica. Italiano e italiani dei social network”.<br />A TEDx Montebelluna ci ha fatti riflettere su come siamo gli unici esseri viventi a possedere la capacità del linguaggio, ma spesso non ci rendiamo conto di questa capacità. Con le parole rappresentiamo la realtà e con le parole possiamo accarezzare e ferire, convincere e respingere. Ci ha spinti ad amare di nuovo la nostra lingua e a riscoprire il potere di usare le parole corrette.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/4OGfpLwJ2Zc?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/BTZq2q_Cicg?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/PROVEDEL.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Renzo Provedel <span>Ogni problema ha una soluzione</span></h2>
                        <p>Renzo Provedel è un coach per le strategie e l’innovazione e ha partecipato allo sviluppo della prima scuola di coaching in Italia (SCOA, a Milano).<br />La sua esperienza come manager si è svolta in ambito ICT (Information and Communication technology) e in Logistica, competenze che ora, da imprenditore, utilizza per facilitare l’innovazione delle grandi imprese, ed è focalizzato sulll’Open Innovation.<br />Oggi è impegnato a lanciare una startup innovativa, BRIT, per il ri-uso di edifici storici. <br />Nel suo intervento a TEDx Montebelluna ha spiegato cosa significa “innovazione aperta”e come si possono trovare nuove soluzioni ai problemi interni a un’azienda o a un settore utilizzando risorse esterne e apparentemente estranee.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/cYA1t5ofC2g?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/Fd32xwvCOXo?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/DECARLO.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Giorgio De Carlo <span>Stupidità del gregge o Intelligenza della folla? - Metodi previsionali per il futuro</span></h2>
                        <p>Giorgio De Carlo è docente al Master universitario in Strategie per il Business dello Sport dell’Università Ca’ Foscari di Venezia.<br />È fondatore e direttore dell’Istituto di ricerca Queris, leader per ricerche di mercato, indagini di marketing strategico e sondaggi d’opinione.<br />Ha collaborato come ricercatore e consulente con il Ministero dell'Istruzione dell'Università e della Ricerca, è stato membro della commissione RAI per il portale RAI Educational e fa parte del gruppo Atlantide.<br />Nel suo intervento a TEDx Montebelluna ha parlato del Prediction Market, un gioco di scommesse competitivo, studiato per sfruttare la capacità dell'intelligenza collettiva di prevedere il futuro.<br />È una tecnica basata sul ruolo dell’intelligenza della folla nel fare previsioni negli ambiti più svariati: il successo di una nuova idea, l’andamento di un nuovo prodotto, il risultato di una competizione elettorale e molto altro.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/i31wSc1ZqS0?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/XXzTf_oqxXM?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/BARCO.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Davide Barco <span>Elogio alla (s)fortuna</span></h2>
                        <p>Davide Barco è un illustratore padovano appassionato di sport. Ha studiato allo IED e ha lavorato come Art director per agenzie pubblicitarie internazionali. Vive e lavora a Milano e ha illustrato, tra gli altri, per NBA, HBO Europe, il New York Times, il Wall Street Journal, Milan, Juventus e American Express.<br />Nella sua divertente talk “Elogio alla sfortuna” ha ripercorso con ironia il suo percorso umano e professionale, di come è diventato un illustratore sbagliando molto come Art director, tra New York, Londra e l’Italia.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/OYfODoXJL3I?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/RQOEA2V16n8?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/VISENTIN.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Andrea Visentin <span>Smart Industry, come l’intelligenza artificiale sta trainando una nuova rivoluzione industriale</span></h2>
                        <p>Andrea Visentin è un Ingegnere informatico ed è partner di un’azienda che si occupa di sviluppo web e di piattaforme di e-learning per farmacie e laboratori farmaceutici. Sta completando un Dottorato di Ricerca in Intelligenza Artificiale applicata all’industria all’Università di Cork, in Irlanda. <br />Per TEDx Montebelluna ha parlato proprio di Intelligenza Artificiale, un termine che vediamo spesso nell’attualità e nei film di fantascienza. Ma quanto è vicina questa rappresentazione a quello che l’Intelligenza Artificiale può davvero fare per noi? In questo speech verranno affrontati temi come il Decision Making e l’industria 4.0 e il modo in cui l’uso dei computer sta cambiando il mondo del lavoro.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/dQhRysratyA?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/7Y4uDIRcrrQ?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/SUMINI.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Valentina Sumini <span>Come progetta un architetto spaziale?</span></h2>
                        <p>Valentina Sumini è Postdoctoral Associate al Massachusetts Institute of Technology di Boston e collabora con diversi gruppi di ricerca: Digital Structures del Dipartimento di Ingegneria civile e ambientale, Tangible Media e Space Exploration Initiative del MIT MediaLab. Nella sua attività di ricerca esplora strategie di ottimizzazione per la ricerca di forme architettoniche per gli habitat di esplorazione spaziale sulla Luna e Marte. La ricerca è applicata a tre diversi scenari: una città su Marte, un villaggio lunare e un hotel di lusso orbitante intorno alla Terra.<br />A TEDx Montebelluna ci ha fatti viaggiare in un futuro non troppo lontano in cui l’uomo comincerà ad abitare su Marte e avrà bisogno di infrastrutture progettate per superare le sfide dell’ambiente extra-planetario.<br />Ci ha fatto conoscere la figura dell’architetto spaziale e capiremo quali sono le dinamiche e gli ostacoli che deve affrontare chi progetta per Marte e come queste soluzioni architettoniche potrebbero aiutarci a raggiungere un futuro più sostenibile anche sulla Terra.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/j57jMrSBXuM?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/Jz2F5qQ_OYY?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/BONACCINI.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Filippo Bonaccini <span>Smart village: un modello di re-urbanizzazione sostenibile</span></h2>
                        <p>Filippo Bonaccini è un ingegnere civile laureto all’Università di Padova e alla University of California di Berkeley.<br />Prima progettista strutturale presso Italstrade, dal 1992 esercita la professione a Treviso. <br />Dopo un periodo in cui si è dedicato alla progettazione strutturale, estende l’attività professionale alla progettazione generale, sia per il settore pubblico che per il privato.<br />Nel 1998 diventa socio fondatore e direttore tecnico di Opera Engineering, che nel 2012 si evolve in Urban Professionals.<br />Per Urban Professionals si occupa di ingegneria energetica, e ha acquisito la certificazione ICMQ come EGE (Esperto nella gestione dell’energia). A TEDx Montebelluna ci ha parlato di Smart District, un modello di re-urbanizzazione sostenibile dei quartieri attraverso la costruzione di modelli innovativi di abitazione e di gestione dell’energia.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/kznQeABv0UM?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/9nmlYRDusA0?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
<hr />

<div class="video-item">
    <div class="video-flex">
        <div class="video-left">
            <div class="video-table">
                <div class="video-cell">
                    <img src="images/speaker/red/MAREGA.jpg" width="100%" />
                </div>
            </div>
        </div>
        <div class="video-right">
            <div class="video-table">
                <div class="video-cell">
                    <div class="talks">
                        <h2>Antonello Marega <span>Montebelluna Sportsystem - Suggested Market Megatrends</span></h2>
                        <p>Antonello Marega è Presidente di EPSI, una piattaforma europea per l'innovazione nello sport. È anche Professore allo IUAV di Venezia nel corso di Design di prodotto e strategia d'impresa.È stato responsabile del reparto Ricerca e Sviluppo per Tecnica S.p.a., e si è occupato del lancio di diversi prodotti nel settore della calzatura sportiva e per il tempo libero, seguendone l'ideazione, lo sviluppo del design, i test e la produzione.<br />Per TEDx Montebelluna ci ha parlato di Circular Economy, o economia circolare, basata sul concetto che gli scarti siano risorse da riutilizzare, e ci ha raccontato le città del futuro, sempre più smart ma abitate da persone sempre più anziane, spesso con grossi problemi di obesità. Insieme a lui abbiamo scoperto anche quali sono le attività motorie consigliate per questo nuovo tipo di popolazione.</p>

                        <div class="row video-row">
                            <div class="col-sm-6">
                                <h3>La presentazione di Franco e Giulio</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/HtAHYkcKGPw?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <h3>La talk</h3>
                                <div class="videoWrapper">
                                    <div class="videoWrapperAspect">
                                        <iframe width="70%" height="300" src="https://www.youtube.com/embed/eXc3Gtt1wxA?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
</div>
</div>
</div>