

<!--====  End of Page Title  ====-->
<style>
h2 {
	font-size: 70px;
}
</style>
<section class="section contact-form no-bottom-line">
	<div class="container">
		<div class="row">
        	<div class="col-sm-12 text-center">
				<h2><span class="alternate">Patrocini</span></h2>
        	</div>
    	</div>
        <div class="container">
			<div class="row partner-subtitle-row">
				<div class="col-sm-4 col-xs-12 text-center"><br /><br />
				<a href="https://www.regione.veneto.it/web/guest" target="_blank"><img src="assets/images/partner/2019/patrocini/veneto.png" width="180" /></a>
				</div>
				<div class="col-sm-4 col-xs-12 text-center"><br /><br />
					<a href="https://www.provincia.treviso.it/" target="_blank"><img src="assets/images/partner/2019/patrocini/treviso.jpeg" width="100" /></a>
				</div>
				<div class="col-sm-4 col-xs-12 text-center"><br />
					<a href="http://www.comune.montebelluna.tv.it/hh/index.php" target="_blank"><img src="assets/images/partner/2019/patrocini/montebelluna.png" width="150" /></a>
				</div>
			</div>
        </div>
	</div>
    <br /><br /><br />

	<div style="background: #FFFFFF; padding-top: 0px; padding-bottom: 40px;">
		<div class="row partner-title-row">
			<div class="col-sm-12 text-center">
				<h2 style="color: #000000;  font-weight: normal;"> <span class="alternate no-italic">Gold</span> partner</h2>
			</div>
    	</div>
		<div class="container">
			<div class="row partner-subtitle-row">
				<div class="col-sm-4 col-xs-6 text-center"><br /><br /><br />
					<a href="http://www.g-g.it/" target="_blank"><img src="assets/images/partner/2019/gold/G_G.png" width="210" /></a>
				</div>
				<div class="col-sm-4 col-xs-6 text-center"><br />
					<a href="http://www.lomges.com/" target="_blank"><img src="assets/images/partner/2019/gold/LomGes.png" width="140" /></a>
				</div>
				<div class="col-sm-4 col-xs-6 text-center"><br /><br /><br /><br />
					<a href="http://www.decastelli.it/it" target="_blank"><img src="assets/images/partner/2019/gold/_dc.png" width="210" /></a>
				</div>
				<div class="col-sm-12"></div><br />
				<div class="col-sm-4 col-xs-6 text-center"><br />
					<br /> <br />
					<a href="http://www.caterinab.it/" target="_blank"><img src="assets/images/partner/2019/gold/CaterinaB.png" width="200" /></a>
					<br /> <br />
				</div>
				<div class="col-sm-4 col-xs-6 text-center"><br />
					<a href="https://www.naturasi.it/" target="_blank"><img src="assets/images/partner/2019/gold/naturasi.png" width="130" /></a>
				</div>
				<div class="col-sm-4 col-xs-6 text-center"><br />
					<a href="http://www.ipssarmaffioli.it/" target="_blank"><img src="assets/images/partner/2019/gold/logo_maffioli_ok.png" width="125" /></a>
				</div>
				<div class="col-sm-12 col-xs-12 text-center"><br />
					<a href="http://www.ascom.tv.it/" target="_blank"><img src="assets/images/partner/2019/gold/confcommercio.png" width="195" /></a>
				</div>
			</div>
			<br /><br /><br /><br />
		</div>
		<div class="row partner-title-row">
			<div class="col-sm-12 text-center">
				<h2 style="color: #000000;  font-weight: normal;"><span class="alternate no-italic">Silver</span> partner</h2>
			</div>
		</div>
		<div class="container">
			<div class="row partner-subtitle-row">
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="http://www.confartigianatoasolomontebelluna.it/" target="_blank"><img src="assets/images/partner/2019/silver/conf.png" width="200" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="https://www.logisticnet.it" target="_blank"><img src="assets/images/partner/2019/silver/logisticnet.png" width="220" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br /><br />
					<a href="https://www.abacospa.it/" target="_blank"><img src="assets/images/partner/2019/silver/abaco.jpeg" width="250" /></a>
				</div>
				<div class="col-sm-12"></div><br />
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br />
					<a href="http://www.maikii.com" target="_blank"><img style="margin-top: 10px;" src="assets/images/partner/2019/silver/maikii.png" height="130" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center">
					<br /><br />
					<a href="https://reisarchitettura.it" target="_blank"><img src="assets/images/partner/2019/silver/reis.jpg" width="320" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br />
					<br />
					<a href="https://www.giustiwine.com" target="_blank"><img src="assets/images/partner/2019/silver/giusti.png" width="190" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br />
					<br /><br />
					<a href="https://piccoloseeds.com/" target="_blank"><img src="assets/images/partner/2019/silver/piccolo.png" width="220" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br /><br />
					<a href="mailto:segreteria@studiofranchin.it" target="_blank"><img src="assets/images/partner/2019/silver/franchin.png" height="150" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br />
					<br /><br /><br /><br />
					<a href="http://www.eliocartotecnica.it/" target="_blank"><img src="assets/images/partner/2019/silver/elio.png" width="320" /></a>
				</div>
				<div class="col-sm-12 col-xs-12 col-md-12 text-center"><br /><br /><br />
					<a href="http://www.bincaffe.it/" target="_blank"><img src="assets/images/partner/2019/silver/bincaffe.png" width="150" /></a>
				</div>
			</div><br /><br /><br /><br />
		</div>
		<div class="row partner-title-row">
			<div class="col-sm-12 text-center">
				<h2 style="color: #000000; font-weight: normal;">Partner <span class="alternate no-italic">tecnici</span></h2>
			</div>
		</div>
		<div class="container">
			<div class="row partner-subtitle-row">
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="https://www.facebook.com/marinaico-1617325261880685/" target="_blank"><img src="assets/images/partner/2019/tecnici/marinai.png" width="160" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br /><br /><br />
					<a href="http://www.caspineda.com" target="_blank"><img style="margin-top: 10px;" src="assets/images/partner/2019/tecnici/spineda.png" width="250" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br />
					<br /><br /><br /><br />
					<a href="https://www.nonnonanni.it" target="_blank"><img src="assets/images/partner/2019/tecnici/nonnonanni.png" width="220" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br />
					<a href="https://www.ic8m.it/" target="_blank"><img src="assets/images/partner/2019/tecnici/icm.png" width="150" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="https://kapuziner.business.site/" target="_blank"><img src="assets/images/partner/2019/tecnici/kapuziner.png" width="190" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="https://www.fordmar-auto.it/" target="_blank"><img src="assets/images/partner/2019/tecnici/marauto.png" width="240" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="https://www.bancadellamarca.it/" target="_blank"><img src="assets/images/partner/2019/tecnici/banca_marca.jpg" width="270" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="https://www.volpaghese.it" target="_blank"><img src="assets/images/partner/2019/tecnici/volpaghese.png" width="240" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-4 text-center"><br /><br />
					<a href="https://www.baseggiocoperture.com/" target="_blank"><img src="assets/images/partner/2019/tecnici/baseggio.png" width="270" /></a>
				</div>
				<div class="col-sm-6 col-xs-6 col-md-12 text-center"><br /><br /><br />
					<a href="https://www.facebook.com/pages/Fioreria-Ninfea/495436110624259" target="_blank"><img src="assets/images/partner/2019/tecnici/ninfea.png" width="180" /></a>
				</div>
			</div>
		</div>
	
		<br /><br /><br /><br />

		<div class="row partner-title-row">
			<div class="col-sm-12 text-center">
			<h2><span class="alternate no-italic">Locations</span></h2>
			</div>
		</div>
		<div class="container" style="padding-top: 40px; padding-bottom: 40px;">
			<div class="row partner-subtitle-row">
				<div class="col-sm-2">

				</div>
				<div class="col-sm-4 text-center"><br />
					<a href="https://www.infinitearea.com" target="_blank"><img src="assets/images/partner/2019/tecnici/infinite_area.png" width="200" /></a>
				</div>
				<div class="col-sm-4 text-center"><br />
					<a href="http://www.cinemamontebelluna.com/" target="_blank"><img src="assets/images/partner/2019/tecnici/cinema.png" width="200" /></a>
				</div>
				<div class="col-sm-2">

				</div>
			</div>

			<br /><br /><br /><br />
		</div>
		<div class="row partner-title-row">
			<div class="col-sm-12 text-center">
				<h2 style="color: #000000; font-weight: normal;"><span class="alternate no-italic">Media</span> partner</h2>
			</div>
		</div>

		<div class="container" style="padding-top: 40px; padding-bottom: 40px;">
			<div class="row partner-subtitle-row">
				<div class="col-sm-4 text-center"><br /><br />
					<a href="https://marketersclub.it/" target="_blank"><img src="assets/images/partner/2019/media/marketers.png" width="200" /></a>
				</div>
				<div class="col-sm-4 text-center"><br />
					<a href="https://www.radiopadova.com/" target="_blank"><img src="assets/images/partner/2019/media/padova.png" width="200" /></a>
				</div>
				<div class="col-sm-4 text-center"><br />
					<a href="https://www.sgaialand.it/" target="_blank"><img src="assets/images/partner/2019/media/sgaialand.png" width="200" /></a>
				</div>
				<div class="col-sm-6 text-center"><br />
					<br />
					<a href="https://trevisosettegiorni.it/" target="_blank"><img src="assets/images/partner/2019/media/mbweek.png" width="240" /></a>
				</div>
				<div class="col-sm-6 text-center"><br />
					<br />
					<a href="https://www.montebellunasportsystem.com/" target="_blank"><img src="assets/images/partner/2019/media/sportsystem.png" width="240" /></a>
				</div>
			</div>
		</div>
	</div>
</section>