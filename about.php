<style>
.about {
	padding-bottom: 0px;
}
</style>

<!--====  End of Page Title  ====-->
<br />

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-6 align-self-center">
				<div class="image-block two bg-about">
					<img class="img-fluid" src="assets/images/ideas_worth.png" alt="">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 align-self-center ml-lg-auto">
				<div class="content-block">
					<h2>Il<span class="alternate">TED</span></h2>
					<div class="description-one">
						<p>
						Il TED (acronimo per Technology, Entertainment, Design) è un'organizzazione noproft che mette in piedi eventi con l’obiettivo di far incontrare le idee più stupefacenti e innovative del pensiero locale e globale.
						</p>
					</div>
					<div class="description-two">
						<p>“Ideas worth spreading”, idee che meritano di essere diffuse, è il motto e la mission di TED. Le idee che circolano hanno la forza di cambiare il comportamento e la vita delle persone e il modo in cui si relazionano l’una con l’altra, creando dei network che a loro volta generano altre idee.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 align-self-center ml-lg-auto">
				<div class="content-block">
					<h2>Far <span class="alternate">circolare le idee</span></h2>
					<div class="description-one">
						<p>
						TED è un evento annuale che raggruppa grandi pensatori di tutto il mondo per condividere idee che contano in diverse disciplina: tecnologia, divertimento, scienza, design, discipline umanistiche, affari, sviluppo.
						</p>
					</div>
					<div class="description-two">
						<p>L’ex presidente USA Bill Clinton, il fondatore di Microsoft Bill Gates, James Dewey Watson, premio Nobel per la scoperta della struttura del DNA. E poi Arianna Huffington, fondatrice dell’Huffington Post, J.K. Rowling, autrice della saga di Harry Potter, il regista J.J. Abrams, l’artista Marina Abramović: questi sono solo alcuni dei nomi degli speaker che hanno dato il loro contributo alle conferenze TED.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-6 align-self-center">
				<div class="image-block two bg-about">
					<img class="img-fluid" src="assets/images/gate.png" alt="">
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about no-bottom-line">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-6 align-self-center">
				<div class="image-block two bg-about">
					<img class="img-fluid" src="assets/images/tedx.png" alt="">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 align-self-center ml-lg-auto">
				<div class="content-block">
					<h2>Il<span class="alternate">TEDx</span></h2>
					<div class="description-one">
						<p>
						TED ha dato la possibilità di realizzare eventi locali, i TEDx, con l’obiettivo di trasmettere lo spirito e l’esperienza TED a un pubblico sempre più vasto.
						</p>
					</div>
					<div class="description-two">
						<p>TEDx è un forum di idee e di incontro costruito intorno a una comunità e a un territorio interessati alla valorizzazione della scienza e della vita dell’uomo. Ecco alcuni degli speaker che negli anni hanno partecipato agli eventi TEDx: la giornalista e scrittrice Mimosa Martini, il divulgatore informatico Salvatore Aranzulla, lo scrittore Alessandro D'Avenia, lo stilista e imprenditore Brunello Cucinelli, l'autore Giulio Rapetti Mogol, l'attore e scrittore Marco Paolini, l'economista Tito Boeri.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div align="center" class="container"><br /><br />
<img class="img-fluid" src="assets/images/numeri_tedx.png" alt="Numeri TEDx"><br /><br /><br /><br /><br /><br />
</div>
</section>