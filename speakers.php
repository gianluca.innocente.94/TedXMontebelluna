

<!--====  End of Page Title  ====-->

<section class="section contact-form no-bottom-line">
	<style>
footer {
    border-top: none;
}

.team-popup-info p {
    line-height: 30px;
}

.team-popup-container {
    padding-top: 0px;
    top: 30px;
}

.no-padding {
	margin-bottom: 30px;
}

</style>
<div class="row" style="position: relative; margin: 0;">

<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_DERAMO"><img src="assets/images/speaker/red/DERAMO.jpg" width="100%" id="image_DERAMO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DERAMO">
                    <div class="team-fast-hover">
                        <h1>Francesco D'Eramo</h1>
                        <h3>Svelare l'Universo invisibile</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DERAMO">

                </div>
            </div>
        </a>
		<br />
    </div>
    
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_PANDOLFI"><img src="assets/images/speaker/red/PANDOLFI.jpg" width="100%" id="image_PANDOLFI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_PANDOLFI">
                    <div class="team-fast-hover">
                        <h1>Camilla Pandolfi</h1>
                        <h3>Piante e bioispirazione</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="PANDOLFI">

                </div>
            </div>
        </a>
		<br />
    </div> 

<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_SANSON"><img src="assets/images/speaker/red/SANSON.jpg" width="100%" id="image_SANSON" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_SANSON">
                    <div class="team-fast-hover">
                        <h1>Antonio Sanson</h1>
                        <h3>Il giro del mondo in barca a vela</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="SANSON">

                </div>
            </div>
        </a>
		<br />
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_TOMAELLO"><img src="assets/images/speaker/red/TOMAELLO.jpg" width="100%" id="image_TOMAELLO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_TOMAELLO">
                    <div class="team-fast-hover">
                        <h1>Andrea Tomaello</h1>
                        <h3>Lo spirito del gioco</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="TOMAELLO">

                </div>
            </div>
        </a>
		<br />
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_MARTONE"><img src="assets/images/speaker/red/MARTONE.jpg" width="100%" id="image_MARTONE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_MARTONE">
                    <div class="team-fast-hover">
                        <h1>Andrea Martone</h1>
                        <h3>Job crafting: il lavoro che vorrei</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="MARTONE">

                </div>
            </div>
        </a>
		<br />
    </div>

<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_BALDO"><img src="assets/images/speaker/red/BALDO.jpg" width="100%" id="image_BALDO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_BALDO">
                    <div class="team-fast-hover">
                        <h1>Ermanno Baldo</h1>
                        <h3>Respirare a 1800 metri</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="BALDO">

                </div>
            </div>
        </a>
		<br />
    </div>

<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_AGOSTINETTI"><img src="assets/images/speaker/red/AGOSTINETTI.jpg" width="100%" id="image_AGOSTINETTI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_AGOSTINETTI">
                    <div class="team-fast-hover">
                        <h1>Piero Agostinetti</h1>
                        <h3>Una cura per Gaia</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="AGOSTINETTI">

                </div>
            </div>
        </a>
		<br />
    </div>

<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_CIVIERO"><img src="assets/images/speaker/red/CIVIERO.jpg" width="100%" id="image_CIVIERO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_CIVIERO">
                    <div class="team-fast-hover">
                        <h1>Stefano Civiero</h1>
                        <h3>Cervello, Emozioni & Marketing. Un viaggio interstellare</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="CIVIERO">

                </div>
            </div>
        </a>
		<br />
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_PEZZULLI"><img src="assets/images/speaker/red/PEZZULLI.jpg" width="100%" id="image_PEZZULLI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_PEZZULLI">
                    <div class="team-fast-hover">
                        <h1>Francesco Pezzulli</h1>
                        <h3>Dai voce ai tuoi pensieri</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="PEZZULLI">

                </div>
            </div>
        </a>
		<br />
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_CORRADINI"><img src="assets/images/speaker/red/CORRADINI.jpg" width="100%" id="image_CORRADINI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_CORRADINI">
                    <div class="team-fast-hover">
                        <h1>Luciano Corradini</h1>
                        <h3>Discepoli e maestri per sempre</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="CORRADINI">

                </div>
            </div>
        </a>
		<br />
    </div>

<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_DATTANASIO"><img src="assets/images/speaker/red/DATTANASIO.jpg" width="100%" id="image_DATTANASIO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DATTANASIO">
                    <div class="team-fast-hover">
                        <h1>Michele D'Attanasio</h1>
                        <h3>Dare luce alle emozioni</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DATTANASIO">

                </div>
            </div>
        </a>
		<br />
    </div>

<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_ZULIANI"><img src="assets/images/speaker/red/ZULIANI.jpg" width="100%" id="image_ZULIANI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ZULIANI">
                    <div class="team-fast-hover">
                        <h1>Nicolò Zuliani</h1>
                        <h3>Giudizio istantaneo</h3>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ZULIANI">

                </div>
            </div>
        </a>
		<br />
    </div>

    <div class="red-back"></div>

    <div class="team-popup-background" id="team_popup" style="display: none;">
        <div class="team-popup-container">
            <div class="col-container">
                <div class="col col-40">
                    <img src="assets/images/speaker/col/MARCOLIN.jpg" width="100%" id="team_popup_image" />
                </div>
                <div class="col col-60" style="padding: 25px;">
                    <div class="team-popup-info">
                        <div>
                            <h1 id="team_popup_title">Andrea Tomaello</h1>
                            <p id="team_popup_description"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="team-popup-left">
                <a href="#" id="team_popup_previous"></a>
            </div>

            <div class="team-popup-right">
                <a href="#" id="team_popup_next"></a>
            </div>

            <div class="team-popup-close">
                <a href="javascript: hidePopup();"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>
<script>
    var team = [
        {
            id: "DERAMO",
            name: "Francesco D'Eramo",
            role: "",
            description: "Laureato in Fisica alla Normale di Pisa e con un dottorato ottenuto al Massachusetts Institute of Technology, è ricercatore all'Università di Padova e associato all'Istituto Nazionale di Fisica Nucleare. <br /><br />I suoi studi ruotano attorno al problema della materia oscura, di cui parlerà il 18 maggio sul palco."
        },
        {
            id: "PANDOLFI",
            name: "Camilla Pandolfi",
            role: "",
            description: "Agronoma e ricercatrice all'Università di Firenze, si occupa di fisiologia vegetale e dei meccanismi di comunicazione tra piante e ambiente, tema di cui ci parlerà il prossimo 18 maggio. <br /><br />Pandolfi è inoltre Marie Curie Fellow e Research Fellow dell\'Agenzia Spaziale Europea"
        },
        {
            id: "SANSON",
            name: "Antonio Sanson",
            role: "",
            description: "Friulano, pizzaiolo stagionale, al mare d'estate e in montagna d'inverno: grazie al suo lavoro Antonio Sanson ha avuto la possibilità di viaggiare molto. <br /><br />In barca a vela ha fatto il giro del mondo e ha toccato i cinque continenti anche via terra e via aria. <br />Dall'età di 23 anni ha iniziato a viaggiare fuori Europa e da lì non si è più fermato."
        },
        {
            id: "TOMAELLO",
            name: "Andrea Tomaello",
            role: "",
            description: "Andrea Tomaello nasce a Venezia nel 1975. Già dai primi anni di vita cresce in ambienti sportivi grazie ai genitori, imparando i concetti di amicizia, rispetto e sacrificio.<br /><br />Dopo aver provato varie discipline, a 25 anni scopre per caso a Rimini i campionati del mondo di Beach Ultimate per club: lì capisce la differenza tra comprare un frisbee da lanciare con gli amici e fondare una vera e propria squadra, vestendo la maglia azzurra in competizioni internazionali. \"È stato un attimo\", come lui confessa. Un attimo che dura da quasi 20 anni e proprio di questo ci parlerà sul palco del 18 maggio."
        },
        {
            id: "MARTONE",
            name: "Andrea Martone",
            role: "",
            description: "Nato in provincia di Caserta, lavora tra la Svizzera e l'Italia, coniugando l'attività accademica con quella professionale. <br /><br />È professore e direttore del Master of Advanced Studies all'Università di Lugano, ricercatore all'Università \"Carlo Cattaneo\" di Castellanza (Varese) e docente all'ateneo francese di Lille. <br /><br />Ha lavorato all'Istituto europeo di design (IED) e il prossimo 18 maggio ci parlerà del fenomeno dello smart working."
        },
        {
            id: "BALDO",
            name: "Ermanno Baldo",
            role: "",
            description: "Medico con una specializzazione in Pediatria e Immunoematologia, è presidente regionale della Società Italiana di Pediatria. <br />Ermanno Baldo è inoltre direttore clinico del Centro per la Cura dell'Asma Infantile di Misurina e proprio di questo innovativo centro e degli studi sulla respirazione ci parlerà sul nostro palco il prossimo 18 maggio.",
        },
        {
            id: "AGOSTINETTI",
            name: "Piero Agostinetti",
            role: "",
            description: "Ricercatore presso il Consiglio Nazionale delle Ricerche, Piero Agostinetti collabora con laboratori sparsi in diverse parti del mondo. <br /><br />Sta lavorando alla fusione nucleare, vista come una delle fonti di energia del futuro, e proprio di questo parlerà sul palco il 18 maggio! <br /><br />Ma non è finita qui! Presto altre novità sulle nostre pagine Facebook e Instagram!",
        },
        {
            id: "CIVIERO",
            name: "Stefano Civiero",
            role: "",
            description: "Specializzato nel fornire alle aziende soluzioni per la comunicazione d'impresa online e offline, Stefano si occupa dell'analisi del comportamento dei consumatori di fronte alle scelte di marketing.<br /><br />Elabora dati per fornire strategie di business e collabora con il Centro Internazionale di Studi sull'Economia Turistica dell'Università Ca' Foscari di Venezia.<br /><br />Svolge test di neuromarketing, argomento che affronterà durante la sua talk. "
        },
        {
            id: "PEZZULLI",
            name: "Francesco Pezzulli",
            role: "",
            description: "Pezzulli studia recitazione fin da bambino e dopo alcune esperienze in tv e teatro approda al doppiaggio.<br />È la voce italiana di Leonardo DiCaprio, per la quale ha vinto nel 2004 il premio Leggio d'Oro con il doppiaggio nel film Titanic. Stesso riconoscimento nel 2013 per la voce prestata all'attore in Django Unchained e Il Grande Gatsby.<br /><br />Francesco Pezzulli è stato inoltre doppiatore nella nota serie tv Dawson's Creek, lavora nella pubblicità ed é una delle voci di Rai Due e Radio Deejay.",
        },
        {
            id: "CORRADINI",
            name: "Luciano Corradini",
            role: "",
            description: "Professore emerito di Pedagogia generale all'Università di Roma Tre, ha insegnato anche alla Statale di Milano e alla Sapienza di Roma.<br /><br />Nel 1995 ha ricevuto la cittadinanza onoraria della città di Praia a Mare, mentre nel 2000 gli è stata conferita la Medaglia d'oro dei benemeriti della scuola, della cultura e dell'arte e sul palco del 18 maggio ci proporrà un percorso di educazione alla cittadinanza!"
        },
        {
            id: "DATTANASIO",
            name: "Michele D'Attanasio",
            role: "",
            description: "Nato a Pescara nel 1976, è un direttore della fotografia autodidatta, passione di cui ci parlerà il prossimo 18 maggio. <br />Ha lavorato con i grandi maestri del cinema italiano come ad esempio Nanni Moretti e Mario Martone e giovani registi innovativi come Gabriele Mainetti e Matteo Rovere. <br />Già vincitore nel 2017 di un David di Donatello, ha ricevuto nel tempo delle nomination ai Nastri d'Argento, ai Ciak d'Oro e ai Globi d'Oro. <br />Questa sera tiferemo per lui: è infatti candidato al David di Donatello 2019 per la miglior fotografia, con il film Capri-Revolution!"
        },
        {
            id: "ZULIANI",
            name: "Nicolò Zuliani",
            role: "",
            description: "Nato a Venezia nel 1980, da marinaio inizia a lavorare alla cronaca bianca alla Nuova Venezia e poi al Gazzettino. Nel 2006 crea un blog delirante e tre anni dopo viene chiamato a Milano per collaborare al progetto di una rivista. Passa a scrivere di fitness su Men’s health, poi di costume e società su Cosmopolitan e per breve tempo di cronaca nera su GQ. Pubblica un libro di aneddoti storici, “La storia la fanno gli idioti” e in tempi non sospetti un fumetto di satira politica. Si specializza in Storia del ‘900 di cui oggi scrive su The Vision"
        }
    ];

    $(document).ready(function() {
        $(".team-hover").hover(function() {
            var element = $("#hover_content_" + this.id);
            var display = element.css("display");

            console.log("display "+display+"  for id "+this.id);

            if(display == 'none') {
                $("#image_"+this.id).attr("src", "assets/images/speaker/col/" + this.id + ".jpg");
                element.show();
                console.log("show "+this.id);
            }
        }, function() {
            var display = this.style.display;

            console.log("hide "+display+"  id "+this.id);

            $("#image_"+this.id).attr("src", "assets/images/speaker/red/" + this.id + ".jpg");
            $("#hover_content_" + this.id).hide();
        });

        $(".team-hover").click(function() {
			console.log("click");
            var itemID = this.id;
            var element = this;
            var userID = itemID.replace("link_", "");

            show(userID);

                var topTarget = $(element).offset().top;

                console.log("Before Footer y:"+$("footer").offset().top+"   topTarget:"+topTarget+"    heightPopup:"+$(".team-popup-container").height());

                if($("footer").offset().top < topTarget + $(".team-popup-container").height() + 90) {
                    topTarget = 90;
                }

                /*$('html, body').animate({
                                scrollTop: topTarget
                            }, 500);*/

                //$("#team_popup").css("top", (topTarget - 90) + "px");

        });
    });

    function show(userID) {
        $("#team_popup").fadeOut(200, function() {
            var userIndex = getUserIndexById(userID);
            var user = team[userIndex];
            var prevUser = userIndex > 0 ? team[userIndex - 1] : team[team.length - 1];
            var nextUser = userIndex < team.length - 1 ? team[userIndex + 1] : team[0];

            $("#team_popup_image").attr("src", "");
            $("#team_popup_image").attr("src", "assets/images/speaker/col/" + userID + ".jpg");
            $("#team_popup_title").html(user.name);
            $("#team_popup_subtitle").html(user.role);
            $("#team_popup_description").html(user.description);

            $("#team_popup_previous").html("< " + prevUser.id);
            $("#team_popup_previous").attr("href", "javascript: show('" + prevUser.id + "');");
            $("#team_popup_next").html(nextUser.id + " >");
            $("#team_popup_next").attr("href", "javascript: show('" + nextUser.id + "');");

            $("#team_popup").fadeIn(200);

            $(".red-back").fadeIn();
        });
    }

    function getUserIndexById(userID) {
        var usrIndex = null;

        for(var i = 0; i < team.length; i++) {
            if(team[i].id == userID) {
                usrIndex = i;
            }
        }

        return usrIndex;
	}
	
	function hidePopup() {
		$('#team_popup').fadeOut();
		$('.red-back').fadeOut();
	}
</script>

	</div>
</section>