

<section class="section contact-form no-bottom-line">
	<div class="">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3>Gallery <span class="alternate"><b>dell'evento</b></span></h3>
				</div>
			</div>
		</div>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<style>
footer {
    border-top: none;
}

</style>
<div class="row" style="position: relative; margin: 0;" id="gallery_box">

</div>
<br />
<br /><br />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
$(document).ready(function() {
    loadImages(0, 36);
});

function loadImages(offset, length) {
    $("#load_more_button").remove();

    var append = "";

    for(i = offset; i < offset + length; i++) {
        append += '<a class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding fancybox" style="height: 200px; background-image:url(assets/images/gallery/f_' + i + '.jpg); background-size: cover;" rel="group" data-fancybox="group" href="assets/images/gallery/f_' + i + '.jpg"></a>';

        if(i > 151) {
            return;
        }
    }

    append += '<div class="col-sm-12 col-xs-12" id="load_more_button"><br /><br /><div align="center"><button class="btn btn-error" onclick="loadImages(' + (offset+length) + ', ' + length + ');">Carica altre immagini</button></div></div>';

    $("#gallery_box").append(append);

    $(".fancybox").fancybox();
}
</script>
	</div>
</section>