<!--============================
=            Banner            =
=============================-->

<section class="banner-two bg-banner-two overlay-white-slant text-overlay" style="background: url(assets/images/vespucci.jpg) no-repeat; background-position: right; background-size: cover;">

	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<!-- Content Block -->
				<div class="block">
				<h1>Buon Vento</h1>
					<h2>18 maggio 2019</h2>
					<h6>@ InfiniteArea, Montebelluna</h6>
					<!-- Action Button -->
					<a href="speakers" class="btn btn-main-md">Scopri gli speaker</a>
				</div>
			</div>
		</div>
	</div>
</section>

<!--====  End of Banner  ====-->

<!--===========================
=            About            =
============================-->

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-6 align-self-center">
				<div class="image-block two bg-about">
					<img class="img-fluid" src="assets/images/buonvento.png" alt="" width="400">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 align-self-center ml-lg-auto">
				<div class="content-block">
					<h2>Buon<span class="alternate">Vento</span></h2>
					<div class="description-one">
						<p>
							Vivere un evento TED significa immergersi in un mare di innovazione.
						</p>
					</div>
					<div class="description-one">
						<p>Ci faremo trasportare dal vento della passione dei nostri speaker e dalla brezza di novità dei vari talk. Per questi motivi, il titolo del TEDxMontebelluna 2019 è: <b>Buon Vento</b>.</p><br />
						<p>"Buon Vento" è un augurio scambiato tra marinai prima di intraprendere un viaggio, affinché si possa raggiungere la meta interessata: una navigazione verso nuove scoperte e intuizioni, proprio come noi intendiamo il TED.</p>
						<br />
						<p><b>Cosa aspetti, sali a bordo e naviga insieme a noi!</b></p>
					</div>
					<!--<ul class="list-inline">
						<li class="list-inline-item">
							<a href="#" class="btn btn-main-md">Buy ticket</a>
						</li>
						<li class="list-inline-item">
							<a href="#" class="btn btn-transparent-md">Read more</a>
						</li>
					</ul>-->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="ted-program">
					<div class="row program-title-row">
						<div class="col-sm-12">
							<h2>Teaser <span class="alternate">ufficiale</span></h2>
						</div>
					</div>
					<br />
					<iframe width="80%" height="675" src="https://www.youtube.com/embed/XHn8D-63Nwc" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section about">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="ted-program">
					<div class="row program-title-row">
						<div class="col-sm-12">
							<h2>Programma <span class="alternate">dell'evento</span></h2>
						</div>
					</div>

					<table>
						<tr>
							<td><div class="program-time">10.00 - 10.45</div></td>
							
							<td class="program-item">Ingresso check-in</td>
						</tr>
						<tr>
							<td><div class="program-time">11.00 - 12.45</div></td>
							
							<td class="program-item">Prima sessione talk</td>
						</tr>
						<tr>
							<td><div class="program-time">13.00 - 14.00</div></td>
							
							<td class="program-item">Pranzo a buffet</td>
						</tr>
						<tr>
							<td><div class="program-time">14.15 - 15.45</div></td>
							
							<td class="program-item">Seconda sessione talk</td>
						</tr>
						<tr>
							<td><div class="program-time">15.45 - 16.30</div></td>
							
							<td class="program-item">Pausa caffè</td>
						</tr>
						<tr>
							<td><div class="program-time">16.30 - 18.00</div></td>
							
							<td class="program-item">Terza sessione talk</td>
						</tr>
						<tr>
							<td><div class="program-time">18.00 - 19.00</div></td>
							
							<td class="program-item">Aperitivo finale</td>
						</tr>

					</table>
					<p>Durante la fase di check-in sarà necessario esibire<br />il biglietto cartaceo o digitale per accedere all’evento.</p>
					<br /><br /><br />
				</div>
			</div>
			<div class="col-sm-6">
				<img src="assets/images/manifesto.png" width="100%" />
			</div>
		</div>
	</div>
</section>

<!--====  End of About  ====-->
<!--
<section class="speakers-full-width">
	<div class="container-fluid p-0">
		<div class="row">
			<div class="col-12">
				<div class="speaker-slider">
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-one.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-two.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-three.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-four.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-five.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-one.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-two.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-three.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-four.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
					<div class="speaker-image">
						<img src="assets/theme/speaker-full-five.jpg" alt="speaker" class="img-fluid">
						<div class="primary-overlay text-center">
							<h5>GEORGE G. HERNANDEZ</h5>
							<p>CEO Rancom Motor</p>
							<ul class="list-inline">
								<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li class="list-inline-item"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>-->

<!--==============================
=            Schedule            =
===============================-->
<!--
<section class="section schedule two">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3>Event <span class="alternate">Schedule</span></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusm tempor incididunt ut labore</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
				<div class="schedule-tab">
					<ul class="nav nav-pills text-center">
					  <li class="nav-item">
					    <a class="nav-link active" href="#nov20" data-toggle="pill">
					    	Day-01
					    	<span>20 November</span>
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#nov21" data-toggle="pill">
							Day-02
					    	<span>21 November</span>
					    </a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" href="#nov22" data-toggle="pill">
							Day-03
					    	<span>22 November</span>
					    </a>
					  </li>
					</ul>
				</div>
			</div>
			<div class="col-lg-10">
				<div class="schedule-contents">
					<div class="tab-content" id="pills-tabContent">
					  <div class="tab-pane fade show active schedule-item" id="nov20">
					  	<ul class="m-0 p-0">
					  		<li class="headings text-center">
					  			<div class="time">Time</div>
					  			<div class="speaker">Speaker</div>
					  			<div class="subject">Subject</div>
					  			<div class="venue">Venue</div>
					  		</li>
					  		
					  		<li class="schedule-details text-center">
					  			<div class="block">
					  				
							  		<div class="time">
							  			<span class="time">9.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Samanta Doe</span>
							  		</div>
							  		
							  		<div class="subject">Introduction to Wp</div>
							  		
							  		<div class="venue">Auditorium A</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">10.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Zerad Pawel</span>
							  		</div>
							  		
							  		<div class="subject">Principle of Wp</div>
							  		
							  		<div class="venue">Auditorium B</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">12.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Henry Mong</span>
							  		</div>
							  		
							  		<div class="subject">Wp Requirements</div>
							  		
							  		<div class="venue">Auditorium C</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">2.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Baily Leo</span>
							  		</div>
							  		
							  		<div class="subject">Introduction to Wp</div>
							  		
							  		<div class="venue">Auditorium D</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">3.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Lee Mun</span>
							  		</div>
							  		
							  		<div class="subject">Useful tips for Wp</div>
							  		
							  		<div class="venue">Auditorium E</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">3.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Lee Mun</span>
							  		</div>
							  		
							  		<div class="subject">Useful tips for Wp</div>
							  		
							  		<div class="venue">Auditorium E</div>
					  			</div>
					  		</li>
					  	</ul>
					  </div>
					  <div class="tab-pane fade schedule-item" id="nov21">
					  	
					  	<ul class="m-0 p-0">
					  		<li class="headings text-center">
					  			<div class="time">Time</div>
					  			<div class="speaker">Speaker</div>
					  			<div class="subject">Subject</div>
					  			<div class="venue">Venue</div>
					  		</li>
					  		
					  		<li class="schedule-details text-center">
					  			<div class="block">
					  				
							  		<div class="time">
							  			<span class="time">9.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Samanta Doe</span>
							  		</div>
							  		
							  		<div class="subject">Introduction to Wp</div>
							  		
							  		<div class="venue">Auditorium A</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">10.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Zerad Pawel</span>
							  		</div>
							  		
							  		<div class="subject">Principle of Wp</div>
							  		
							  		<div class="venue">Auditorium B</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">12.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Henry Mong</span>
							  		</div>
							  		
							  		<div class="subject">Wp Requirements</div>
							  		
							  		<div class="venue">Auditorium C</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">2.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Baily Leo</span>
							  		</div>
							  		
							  		<div class="subject">Introduction to Wp</div>
							  		
							  		<div class="venue">Auditorium D</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">3.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Lee Mun</span>
							  		</div>
							  		
							  		<div class="subject">Useful tips for Wp</div>
							  		
							  		<div class="venue">Auditorium E</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">3.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Lee Mun</span>
							  		</div>
							  		
							  		<div class="subject">Useful tips for Wp</div>
							  		
							  		<div class="venue">Auditorium E</div>
					  			</div>
					  		</li>
					  	</ul>
					  </div>
					  <div class="tab-pane fade schedule-item" id="nov22">
					  	
					  	<ul class="m-0 p-0">
					  		<li class="headings text-center">
					  			<div class="time">Time</div>
					  			<div class="speaker">Speaker</div>
					  			<div class="subject">Subject</div>
					  			<div class="venue">Venue</div>
					  		</li>
					  		
					  		<li class="schedule-details text-center">
					  			<div class="block">
					  				
							  		<div class="time">
							  			<span class="time">9.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Samanta Doe</span>
							  		</div>
							  		
							  		<div class="subject">Introduction to Wp</div>
							  		
							  		<div class="venue">Auditorium A</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">10.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Zerad Pawel</span>
							  		</div>
							  		
							  		<div class="subject">Principle of Wp</div>
							  		
							  		<div class="venue">Auditorium B</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">12.00 AM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Henry Mong</span>
							  		</div>
							  		
							  		<div class="subject">Wp Requirements</div>
							  		
							  		<div class="venue">Auditorium C</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">2.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Baily Leo</span>
							  		</div>
							  		
							  		<div class="subject">Introduction to Wp</div>
							  		
							  		<div class="venue">Auditorium D</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">3.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Lee Mun</span>
							  		</div>
							  		
							  		<div class="subject">Useful tips for Wp</div>
							  		
							  		<div class="venue">Auditorium E</div>
					  			</div>
					  		</li>
					  		
					  		<li class="schedule-details">
					  			<div class="block">
						  			
							  		<div class="time">
							  			<span class="time">3.00 PM</span>
							  		</div>
							  		
							  		<div class="speaker">
										<span class="name">Lee Mun</span>
							  		</div>
							  		
							  		<div class="subject">Useful tips for Wp</div>
							  		
							  		<div class="venue">Auditorium E</div>
					  			</div>
					  		</li>
					  	</ul>
					  </div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
</section>-->

<!--====  End of Schedule  ====-->

<!--=============================
=            Feature            =
==============================-->
<!--
<section class="ticket-feature">
	<div class="container-fluid m-0 p-0">
		<div class="row p-0 m-0">
			<div class="col-lg-7 p-0 m-0">
				<div class="block bg-timer overlay-dark text-center">
					<div class="section-title white m-0">
						<h3>Le conferenze <span class="alternate">TED</span></h3>
						<p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusm</p>
					</div>
					
					<div class="timer"></div>
					<a href="#" class="btn btn-main-md">Buy Ticket</a>
				</div>
			</div>
			<div class="col-lg-5 p-0">
				<div class="block-2">
					<div class="row no-gutters">
						<div class="col-6">
							
							<div class="service-item">
								<i class="fa fa-microphone"></i>
								<h5>8 Speakers</h5>
							</div>
						</div>
						<div class="col-6">
							
							<div class="service-item">
								<i class="fa fa-flag"></i>
								<h5>500 + Seats</h5>
							</div>
						</div>
						<div class="col-6">
							
							<div class="service-item">
								<i class="fa fa-ticket"></i>
								<h5>300 tickets</h5>
							</div>
						</div>
						<div class="col-6">
							
							<div class="service-item">
								<i class="fa fa-calendar"></i>
								<h5>3 days event</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
-->
<!--====  End of Feature  ====-->


<!--==========================
=            News            =
===========================-->
<!--
<section class="news-hr section mb-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3><span class="alternate">News</span></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, enim.</p>
				</div>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-lg-6">
				<article class="news-post-hr">
					<div class="post-thumb">
						<a href="news-single.html">
							<img src="assets/theme/post-thumb-hr-one.jpg" alt="post-image" class="img-fluid">
						</a>
					</div>
					<div class="post-contents border-top">
						<div class="post-title"><h6><a href="news-single.html">Default title here</a></h6></div>
						<div class="post-exerpts">
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed doeiuse tempor incididunt ut
							</p>
						</div>

						<div class="date">
							<h4>20<span>May</span></h4>
						</div>
						<div class="more">
							<a href="news-single.html">Show more</a>
						</div>
					</div>
				</article>
			</div>
			<div class="col-lg-6">
				<article class="news-post-hr">
					<div class="post-thumb">
						<a href="news-single.html">
							<img src="assets/theme/post-thumb-hr-two.jpg" alt="post-image" class="img-fluid">
						</a>
					</div>
					<div class="post-contents border-top">
						<div class="post-title"><h6><a href="news-single.html">Default title here</a></h6></div>
						<div class="post-exerpts">
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed doeiuse tempor incididunt ut
							</p>
						</div>
						
						<div class="date">
							<h4>20<span>May</span></h4>
						</div>
						<div class="more">
							<a href="news-single.html">Show more</a>
						</div>
					</div>
				</article>
			</div>
			<div class="col-lg-6">
				<article class="news-post-hr">
					<div class="post-thumb">
						<a href="news-single.html">
							<img src="assets/theme/post-thumb-hr-three.jpg" alt="post-image" class="img-fluid">
						</a>
					</div>
					<div class="post-contents">
						<div class="post-title"><h6><a href="news-single.html">Default title here</a></h6></div>
						<div class="post-exerpts">
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed doeiuse tempor incididunt ut
							</p>
						</div>
						
						<div class="date">
							<h4>20<span>May</span></h4>
						</div>
						<div class="more">
							<a href="news-single.html">Show more</a>
						</div>
					</div>
				</article>
			</div>
			<div class="col-lg-6">
				<article class="news-post-hr">
					<div class="post-thumb">
						<a href="news-single.html">
							<img src="assets/theme/post-thumb-hr-four.jpg" alt="post-image" class="img-fluid">
						</a>
					</div>
					<div class="post-contents">
						<div class="post-title"><h6><a href="news-single.html">Default title here</a></h6></div>
						<div class="post-exerpts">
							<p>Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed doeiuse tempor incididunt ut
							</p>
						</div>
						
						<div class="date">
							<h4>20<span>May</span></h4>
						</div>
						<div class="more">
							<a href="news-single.html">Show more</a>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</section>
-->
<!--====  End of News  ====-->

<!--=============================
=            Gallery            =
==============================-->
<!--
<section class="gallery-full section pb-0">
	<div class="container-fluid p-0">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3>TEDx <span class="alternate">Gallery</span></h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, autem.</p>
				</div>
			</div>
		</div>
		<div class="row no-gutters">
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-one.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-one.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-two.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-two.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-three.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-three.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-four.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-four.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-five.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-five.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-six.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-six.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-seven.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-sven.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-4">
				<div class="image">
					<img src="assets/theme/gallery-full-eight.jpg" alt="gallery-image" class="img-fluid">
					<div class="primary-overlay">
        				<a class="image-popup" data-effect="mfp-with-zoom" href="assets/theme/gallery-full-popup-eight.jpg"><i class="fa fa-picture-o"></i></a>
        			</div>
				</div>
			</div>
		</div>
	</div>
</section>
-->
<!--====  End of Gallery  ====-->

<!--===================================
=            Pricing Table            =
====================================-->
<!--
<section class="section pricing two">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3>Get <span class="alternate">ticket</span></h3>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusm tempor incididunt ut labore</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6">
				
				<div class="pricing-item">
					<div class="pricing-heading text-center">
						
						<div class="title">
							<h6>Starter</h6>
						</div>
						
						<div class="price">
							<h2>$39.00</h2>
						</div>
					</div>
					<div class="pricing-body">
						
						<ul class="feature-list m-0 p-0">
							<li><p><span class="fa fa-check-circle available"></span>1 Comfortable Seats</p></li>
							<li><p><span class="fa fa-check-circle available"></span>Free Lunch and Coffee</p></li>
							<li><p><span class="fa fa-times-circle unavailable"></span>Certificate</p></li>
							<li><p><span class="fa fa-times-circle unavailable"></span>Easy Access</p></li>
						</ul>
					</div>
					<div class="pricing-footer text-center">
						<a href="#" class="btn btn-transparent-md">Buy a ticket</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				
				<div class="pricing-item featured">
					<div class="pricing-heading text-center">
						
						<div class="title">
							<h6>Standard</h6>
						</div>
						
						<div class="price">
							<h2>$49.00</h2>
						</div>
					</div>
					<div class="pricing-body">
						
						<ul class="feature-list m-0 p-0">
							<li><p><span class="fa fa-check-circle available"></span>1 Comfortable Seats</p></li>
							<li><p><span class="fa fa-check-circle available"></span>Free Lunch and Coffee</p></li>
							<li><p><span class="fa fa-check-circle available"></span>Certificate</p></li>
							<li><p><span class="fa fa-times-circle unavailable"></span>Easy Access</p></li>
						</ul>
					</div>
					<div class="pricing-footer text-center">
						<a href="#" class="btn btn-main-md">Buy a ticket</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 m-auto">
				
				<div class="pricing-item">
					<div class="pricing-heading text-center">
						
						<div class="title">
							<h6>Platinum</h6>
						</div>
						
						<div class="price">
							<h2>$99.00</h2>
						</div>
					</div>
					<div class="pricing-body">
						
						<ul class="feature-list m-0 p-0">
							<li><p><span class="fa fa-check-circle available"></span>1 Comfortable Seats</p></li>
							<li><p><span class="fa fa-check-circle available"></span>Free Lunch and Coffee</p></li>
							<li><p><span class="fa fa-check-circle available"></span>Certificate</p></li>
							<li><p><span class="fa fa-check-circle available"></span>Easy Access</p></li>
						</ul>
					</div>
					<div class="pricing-footer text-center">
						<a href="#" class="btn btn-transparent-md">Buy a ticket</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
-->
<!--====  End of Pricing Table  ====-->

<!--================================
=            Google Map            =
=================================-->

<section class="map new">
	
	<div id="map"></div>
	<div class="address-block">
		<h4>Infinite Area</h4>
		<ul class="address-list p-0 m-0">
			<li><i class="fa fa-home"></i><span>Via San Gaetano, 113/A, <br>Montebelluna, Treviso</span></li>
		</ul>
		<a href="https://www.google.com/maps/place/InfiniteArea/@45.7616712,12.0468537,17z/data=!3m1!4b1!4m5!3m4!1s0x47792502b87be431:0x87b96f061f7a7a16!8m2!3d45.7616712!4d12.0490424" class="btn btn-white-md" target="_blank">OTTIENI LE INDICAZIONI</a>
	</div>
	<div class="register overlay-dark bg-registration-two">
		<div class="block">
			<div class="title text-center">
				<h3><span class="alternate">Ogni idea</span><br /><span class="alternate">ha bisogno di un</span> <i>luogo</i></h3><br />
				<a href="https://infinitearea.com/spazi/" target="_blank"><img src="assets/images/infinite_area.png" width="200" /></a>
			</div>
		</div>
	</div>
</section>

<!--====  End of Google Map  ====-->


<!-- Google Mapl -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCciyEUCT8mAO4FQTIPReHmfDNMwSw4gSA"></script>
<script type="text/javascript" src="assets/theme/gmap.js"></script>