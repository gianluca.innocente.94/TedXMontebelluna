

<!--====  End of Page Title  ====-->

<section class="section contact-form no-bottom-line">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title">
					<h3>Diventa nostro <span class="alternate"><b>Partner</b></span></h3>
					<p>La nostra rete dei partner è composta da aziende, enti, istutizioni, associazioni e media che condividono la visione del TED e sono convinti che le idee migliori possono cambiare il mondo.</p>
					<br>
					<p>Supporta TEDxMontebelluna</p>
				</div>
			</div>
		</div>
		<form action="#" class="row" id="sponsor_form">
			<div class="col-md-6">
				<input type="text" class="form-control main" name="name" id="name" placeholder="Nome">
			</div>
			<div class="col-md-6">
				<input type="email" class="form-control main" name="email" id="email" placeholder="Email">
			</div>
			<div class="col-md-12">
				<input type="text" class="form-control main" name="phone" id="phone" placeholder="Telefono">
			</div>
			<div class="col-md-12">
				<textarea name="message" id="message" class="form-control main" rows="10" placeholder="Note"></textarea>
			</div>
			<div class="col-sm-12 text-center">
                <div align="center">
                    <div class="g-recaptcha" data-sitekey="6LfWq1EUAAAAAHHeSpddiTlCJmMzStQ9uBfqJz7a"></div>
                </div>
            </div>
			<div class="col-12 text-center">
				<button type="submit" class="btn btn-main-md">Invia</button>
			</div>
			<div class="col-12 text-center">
				<div id="sponsor_message" class="form-message"></div>
			</div>
		</form>
	</div>
</section>
<script>
	$(document).ready(function() {
        $("#sponsor_form").submit(function() {
            $("#sponsor_message").hide();

            var params = {
                name: $("#name").val(),
                email: $("#email").val(),
                phone: $("#phone").val(),
                notes: $("#message").val(),
                captcha_response: $("#g-recaptcha-response").val()
            };

            $.post("core/sponsor.php", params, function (data) {
                $("#sponsor_message").html(data);
                $("#sponsor_message").fadeIn();

                return false;
            }).fail(function (error) {
                $("#sponsor_message").html("Si è verificato un errore imprevisto. Ti preghiamo di contattarci scrivendo a partner@tedxmontebelluna.com");

                $("#sponsor_message").fadeIn();
            });

            return false;
        });

        $("#teaser_modal").on('hidden.bs.modal', function (e) {
            $("#teaser_modal iframe").attr("src", $("#teaser_modal iframe").attr("src"));
        });
	});
</script>