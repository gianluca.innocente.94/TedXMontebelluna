<div class="modal fade" id="teaser_modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background: #000000">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #FFFFFF; opacity: 1 !important;">&times;</span>
                </button>
                <h4>Teaser TEDxMontebelluna</h4>
            </div>
            <div class="modal-body">
            <iframe width="100%" height="615" src="https://www.youtube.com/embed/A0k1l_oHe-4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>