<div class="modal modal-wide fade" id="sponsor_modal">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #000000">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #FFFFFF; opacity: 1 !important;">&times;</span>
                </button>
                <h4 class="modal-title">Unisciti a noi, diventa partner!</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="sponsor_form">
                    <div class="modal-body">
                        <div class="box-body row">
                            <div class="col-sm-6">
                                <br />
                                <div>
                                    <label for="sponsor_name">Nome*</label>
                                    <input type="text" class="form-control" id="sponsor_name" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <br />
                                <div>
                                    <label for="sponsor_surname">Cognome*</label>
                                    <input type="text" class="form-control" id="sponsor_surname" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <br />
                                <div>
                                    <label for="sponsor_email">E-mail*</label>
                                    <input type="text" class="form-control" id="sponsor_email" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <br />
                                <div>
                                    <label for="sponsor_phone">Telefono*</label>
                                    <input type="text" class="form-control" id="sponsor_phone" required>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <br />
                                <div>
                                    <label for="sponsor_notes">Note</label>
                                    <textarea class="form-control" id="sponsor_notes"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <br />
                                <div>
                                    <div class="g-recaptcha" data-sitekey="6LfWq1EUAAAAAHHeSpddiTlCJmMzStQ9uBfqJz7a"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-primary">Invia richiesta</button>
                        <br /><br />
                        <div class="modal-message" id="sponsor_message" style="display: none"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>