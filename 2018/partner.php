<style>
.page {
    background: #ffffff;
}
</style>
<div class="partner-container">
<div class="row partner-title-row">
        <div class="col-sm-12">
            <h1>Patrocini</h1>
        </div>
    </div>
    <br /><br />
        <div class="container">
        <div class="row partner-subtitle-row">
            <div class="col-sm-4 col-xs-12"><br /><br />
            <a href="https://www.regione.veneto.it/web/guest" target="_blank"><img src="images/partner/patrocini/veneto.png" width="180" /></a>
            </div>
            <div class="col-sm-4 col-xs-12"><br /><br />
                <a href="https://www.provincia.treviso.it/" target="_blank"><img src="images/partner/patrocini/treviso.jpeg" width="100" /></a>
            </div>
            <div class="col-sm-4 col-xs-12"><br />
                <a href="http://www.comune.montebelluna.tv.it/hh/index.php" target="_blank"><img src="images/partner/patrocini/montebelluna.png" width="150" /></a>
            </div>
        </div>
        </div>

    <br /><br /><br />

    <div class="row partner-title-row">
        <div class="col-sm-12">
            <h1>Partner</h1>
        </div>
    </div>
    <div class="container">
        <div class="row partner-subtitle-row">
            <div class="col-sm-12"><br />
                <h2>Gold partner</h2>
            </div>
            <div class="col-sm-4 col-xs-6"><br />
                <a href="http://www.g-g.it/" target="_blank"><img src="images/partner/gold/geg.png" width="210" /></a>
            </div>
            <div class="col-sm-4 col-xs-6"><br />
                <a href="http://www.lomges.com/" target="_blank"><img src="images/partner/gold/lomges.png" width="200" /></a>
            </div>
            <div class="col-sm-4 col-xs-6"><br />
                <a href="https://www.craispesaonline.it" target="_blank"><img src="images/partner/gold/crai.png" width="210" /></a>
            </div>
            <div class="col-sm-12"></div><br />
            <div class="col-sm-4 col-xs-6"><br />
                <br /> <br />
                <a href="http://www.decastelli.it/it" target="_blank"><img src="images/partner/gold/decastelli.png" width="220" /></a>
                <br /> <br />
            </div>
            <div class="col-sm-4 col-xs-6"><br />
                <a href="http://www.elektrasrl.com/" target="_blank"><img src="images/partner/gold/elektra.png" width="210" /></a>
            </div>
            <div class="col-sm-4 col-xs-6"><br />
                <a href="http://www.ipssarmaffioli.it/" target="_blank"><img src="images/partner/gold/maffioli.png" width="145" /></a>
            </div>
        </div>
        <br /><br />
        <div class="row partner-subtitle-row">
            <div class="col-sm-12"><br />
                <h2>Silver partner</h2>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <a href="http://www.confartigianatoasolomontebelluna.it/" target="_blank"><img src="images/partner/silver/confartigianato.png" width="200" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <a href="http://www.ascom.tv.it/" target="_blank"><img src="images/partner/silver/confcommercio.png" width="220" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <a href="http://www.giustiwine.com/" target="_blank"><img src="images/partner/silver/giusti.png" width="200" /></a>
            </div>
            <div class="col-sm-12"></div><br />
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <a href="https://www.ic8m.it/" target="_blank"><img src="images/partner/silver/icm.png" height="130" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <br /><br /><br />
                <a href="https://www.extremeprinting.it/" target="_blank"><img src="images/partner/silver/extreme.png" height="50" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <br /><br />
                <a href="https://www.facebook.com/gimasalegnoarredo/" target="_blank"><img src="images/partner/silver/gimasa.jpg" width="100" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-12"><br />
                <br /><br />
                <a href="http://www.levelgloves.com/" target="_blank"><img src="images/partner/silver/LEVEL.png" width="220" /></a>
            </div>
        </div>
        <br /><br />
        <div class="row partner-subtitle-row">
            <div class="col-sm-12"><br />
                <h2>Partner tecnici</h2>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br /><br /><br />
                <a href="http://www.caspineda.com" target="_blank"><img src="images/partner/tecnici/caspineda.png" width="250" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <a href="https://www.facebook.com/CircoloPrivatoMezanino/" target="_blank"><img src="images/partner/tecnici/mezanino.png" width="220" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <br />
                <a href="http://www.villaserena.eu/" target="_blank"><img src="images/partner/tecnici/serena.png" width="220" /></a>
            </div>
            <div class="col-sm-12"></div><br />
            <div class="col-sm-6 col-xs-6 col-md-4"><br />
                <a href="mailto:segreteria@studiofranchin.it" target="_blank"><img src="images/partner/tecnici/franchin.png" width="200" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br /><br /><br /><br />
                <a href="http://www.eliocartotecnica.it/" target="_blank"><img src="images/partner/tecnici/eliocartotecnica.png" width="250" /></a>
            </div>
            <div class="col-sm-6 col-xs-6 col-md-4"><br /><br /><br /><br />
                <a href="http://www.bancadellamarca.it/" target="_blank"><img src="images/partner/tecnici/banca_marca.jpg" width="250" /></a>
            </div>
        </div>
        <br /><br />
    </div>

    <div class="row partner-title-row">
        <div class="col-sm-12">
            <h1>Location</h1>
        </div>
    </div>
    <br /><br />
        <div class="row partner-subtitle-row">
            <div class="col-sm-2">

            </div>
            <div class="col-sm-4"><br />
                <a href="https://www.infinitearea.com" target="_blank"><img src="images/partner/tecnici/infinite_area.png" width="200" /></a>
            </div>
            <div class="col-sm-4"><br />
                <a href="http://www.cinemamontebelluna.com/" target="_blank"><img src="images/partner/tecnici/cinema.png" width="200" /></a>
            </div>
            <div class="col-sm-2">

            </div>
        </div>

    <br /><br /><br />

     <div class="row partner-title-row">
        <div class="col-sm-12">
            <h1>Media Partner</h1>
        </div>
    </div>
    <br /><br />
    <div class="row partner-subtitle-row">
        <div class="col-sm-3"><br />
            <a href="https://marketersclub.it/" target="_blank"><img src="images/partner/media/marketers.png" width="200" /></a>
        </div>
        <div class="col-sm-3"><br />
            <a href="https://www.radiopadova.com/" target="_blank"><img src="images/partner/media/radiopadova.png" width="200" /></a>
        </div>
        <div class="col-sm-3"><br />
            <a href="https://www.sgaialand.it/" target="_blank"><img src="images/partner/media/sgaialand.png" width="200" /></a>
        </div>
        <div class="col-sm-3"><br />
            <br />
            <a href="https://www.montebellunasportsystem.com/" target="_blank"><img src="images/partner/media/sportsystem.png" width="240" /></a>
        </div>
    </div>
    <div class="container">
        <div class="row partner-subtitle-row">
            <div class="col-sm-12"><br />

            </div>
        </div>
    </div>
</div>
<br /><br />
