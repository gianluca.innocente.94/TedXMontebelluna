<?php
function get_title($slug) {
    if($slug == "contact") {
        return "Contatti - TEDxMontebelluna";
    } elseif($slug == "about") {
        return "About TED - TEDxMontebelluna";
    } elseif($slug == "Gallery") {
        return "Gallery - TEDxMontebelluna";
    } elseif($slug == "partner") {
        return "Partner - TEDxMontebelluna";
    } elseif($slug == "speaker") {
        return "Speaker - TEDxMontebelluna";
    } elseif($slug == "talk") {
        return "Le talk - TEDxMontebelluna";
    } elseif($slug == "team") {
        return "Team - TEDxMontebelluna";
    } else {
        return "TEDxMontebelluna - Sito Ufficiale";
    }
}
?>