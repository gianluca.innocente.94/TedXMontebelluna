<?php
require("seo.php");

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "home";
}
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8" />
    <title><?php echo get_title($page); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna" />
    <meta name="keywords" content="tedx montebelluna, ted montebelluna" />
    <meta property="og:title" content="TEDxMontebelluna, evento TED organizzato in modo indipendente." />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="http://tedxmontebelluna.com/social.jpg" />
    <meta property="og:url" content="http://www.tedxmontebelluna.com" />
    <meta property="og:locale" content="it_IT" />

    <link rel="apple-touch-icon" sizes="57x57" href="images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="manifest" href="images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ff2b06">
    <meta name="msapplication-TileImage" content="images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ff2b06">

    <link href="https://fonts.googleapis.com/css?family=Cantarell:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Fjalla+One" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">
    <link href="css/style.min.css?<?php echo time(); ?>" rel="stylesheet" type="text/css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="js/main.min.js?3"></script>
</head>

<body>
    <div class="page container-fluid" style="padding-left: 0px; padding-right: 0px;">
        <div class="header-box">
            <header class="container">
                <div class="row">
                    <div class="col-md-11 ted-header-left">
                        <div class="ted-logo">
                            <a href="home"><img height="60" src="images/logo_white.png" alt="Logo TedX Montebelluna" /></a>
                        </div>
                    </div>
                    <div class="col-md-1 ted-header-right">
                        <!--<a href="https://www.eventbrite.it/e/biglietti-tedxmontebelluna-idee-in-orbita-45428340389?aff=ebdssbdestsearch" target="_blank"><button class="btn-buy only-desktop">ACQUISTA IL BIGLIETTO</button></a>-->
                        <a href="gallery"><button class="btn-buy only-desktop">VEDI LA GALLERY</button></a>
                        <div>
                            <a href="javascript: showMenu();">
                                <div class="ted-menu">
                                    <i class="fa fa-bars"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </header>
        </div>

        <?php
include $page . ".php";
?>

        <footer>
            <div class="container ted-footer-menu">
                <ul>
                    <li><a href="home">Home</a></li>
                    <li><a href="speaker">Speaker</a></li>
                    <li><a href="partner">Partner</a></li>
                    <li><a href="talk">Le talk</a></li>
                    <li><a href="gallery">Gallery</a></li>
                    <li><a href="about">About Ted</a></li>
                    <li><a href="team">Chi siamo</a></li>
                    <li><a href="contact">Contatti</a></li>
                </ul>
            </div>
            <div class="container ted-footer-contacts">
                <p class="ted-footer-social">
                    <a href="https://www.facebook.com/tedxmontebelluna/" target="_blank"><i class="fab fa-facebook-f"></i></a> &nbsp; <a href="https://www.instagram.com/tedxmontebelluna/" target="_blank"><i class="fab fa-instagram"></i></a>
                </p>
                <p>Comitato Promotore TEDxMontebelluna<br />
                    Piazza Oberkochen 25/4 - Montebelluna (TV) 31044<br />
                    C.F. 92044330261 - P.IVA 04938770262<br />
                    <a href="mailto:info@tedxmontebelluna.com">info@tedxmontebelluna.com</a>
                </p>
            </div>
            <div class="ted-credits">
                <div class="row">
                    <div class="col-md-4 text-left">
                        © TEDxMontebelluna - All rights reserved
                    </div>
                    <div class="col-md-4 text-center">
                        This independent TEDx event is operated under license from TED.
                    </div>
                    <div class="col-md-4 text-right">
                        designed by <a target="_blank" href="#">Maremoto Studio</a> - developed by <a target="_blank" href="https://www.igsolutions.it">IG Solutions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div class="ted-fixed-menu" id="ted_fixed_menu">
        <div class="ted-fixed-menu-close">
            <a href="javascript: hideMenu();"><i class="fa fa-times"></i></a>
        </div>
        <div class="ted-fixed-menu-table-layout">
            <div class="ted-fixed-menu-list">
                <ul>
                    <li><a href="home">Home</a></li>
                    <li><a href="speaker">Speaker</a></li>
                    <li><a href="partner">Partner</a></li>
                    <li><a href="talk">Le talk</a></li>
                    <li><a href="gallery">Gallery</a></li>
                    <li><a href="about">About Ted</a></li>
                    <li><a href="team">Chi siamo</a></li>
                    <li><a href="contact">Contatti</a></li>
                </ul>
            </div>
        </div>
    </div>

    <?php
include("tickets_modal.php");
?>

<script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
    <script>

        window.sr = new ScrollReveal({reset:true});
sr.reveal('.ted-program tr', { duration: 700 });
</script>

    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109550788-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109550788-1');
        </script>
    <?php endif;?>

</body>

</html>
