function showMenu() {
    $("#ted_fixed_menu").fadeIn();
}

function hideMenu() {
    $("#ted_fixed_menu").fadeOut();
}

function showModal(modalID) {
    $('#' + modalID).modal({
        show: true,
        backdrop: 'static'
    });
}

function hideModal(modalID) {
    $('#' + modalID).modal("hide");
}

function hidePopup() {
    $('#team_popup').fadeOut();
    $('.red-back').fadeOut();
}