<style>
footer {
    border-top: none;
}
</style>
<div class="row" style="position: relative; margin: 0;">
    
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ANDREA"><img src="images/team/black/ANDREA.jpg" width="100%" id="image_ANDREA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ANDREA">
                    <div class="team-fast-hover">
                        <h1>Andrea Franchin</h1>
                        <h2>Licenziatario e organizzatore</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ANDREA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_EMILIANO"><img src="images/team/black/EMILIANO.jpg" width="100%" id="image_EMILIANO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_EMILIANO">
                    <div class="team-fast-hover">
                        <h1>Emiliano Guerra</h1>
                        <h2>Coordinatore partner</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="EMILIANO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_DARIO"><img src="images/team/black/DARIO.jpg" width="100%" id="image_DARIO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DARIO">
                    <div class="team-fast-hover">
                        <h1>Dario Merlo</h1>
                        <h2>Art director</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DARIO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_BEATRICE"><img src="images/team/black/BEATRICE.jpg" width="100%" id="image_BEATRICE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_BEATRICE">
                    <div class="team-fast-hover">
                        <h1>Beatrice Pesente</h1>
                        <h2>Copywriter e Social media manager</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="BEATRICE">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ALBERTO"><img src="images/team/black/ALBERTO.jpg" width="100%" id="image_ALBERTO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ALBERTO">
                    <div class="team-fast-hover">
                        <h1>Alberto Moretto</h1>
                        <h2>Comunicazione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ALBERTO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ELENA"><img src="images/team/black/ELENA.jpg" width="100%" id="image_ELENA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ELENA">
                    <div class="team-fast-hover">
                        <h1>Elena Bressan</h1>
                        <h2>Coordinatore speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ELENA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ENRICO"><img src="images/team/black/ENRICO.jpg" width="100%" id="image_ENRICO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ENRICO">
                    <div class="team-fast-hover">
                        <h1>Enrico Poloni</h1>
                        <h2>Coordinatore partner</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ENRICO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ERIKA"><img src="images/team/black/ERIKA.jpg" width="100%" id="image_ERIKA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ERIKA">
                    <div class="team-fast-hover">
                        <h1>Erika Martinazzo</h1>
                        <h2>Coordinatore speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ERIKA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FABIO"><img src="images/team/black/FABIO.jpg" width="100%" id="image_FABIO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FABIO">
                    <div class="team-fast-hover">
                        <h1>Fabio Donà</h1>
                        <h2>Coordinatore speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FABIO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FEDERICA"><img src="images/team/black/FEDERICA.jpg" width="100%" id="image_FEDERICA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FEDERICA">
                    <div class="team-fast-hover">
                        <h1>Federica Favero</h1>
                        <h2>Responsabile allestimenti</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FEDERICA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FRANCESCA"><img src="images/team/black/FRANCESCA.jpg" width="100%" id="image_FRANCESCA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FRANCESCA">
                    <div class="team-fast-hover">
                        <h1>Francesca Celato</h1>
                        <h2>Coordinatore speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FRANCESCA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FRANCESCO"><img src="images/team/black/FRANCESCO.jpg" width="100%" id="image_FRANCESCO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FRANCESCO">
                    <div class="team-fast-hover">
                        <h1>Francesco Calzamatta</h1>
                        <h2>Comunicazione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FRANCESCO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_GIANLUCA"><img src="images/team/black/GIANLUCA.jpg" width="100%" id="image_GIANLUCA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_GIANLUCA">
                    <div class="team-fast-hover">
                        <h1>Gianluca Innocente</h1>
                        <h2>Web developer</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="GIANLUCA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_MYRNA"><img src="images/team/black/MYRNA.jpg" width="100%" id="image_MYRNA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_MYRNA">
                    <div class="team-fast-hover">
                        <h1>Myrna Isetta</h1>
                        <h2>Responsabile traduzioni</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="MYRNA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_DANIELE"><img src="images/team/black/DANIELE.jpg" width="100%" id="image_DANIELE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DANIELE">
                    <div class="team-fast-hover">
                        <h1>Daniele Bozzano</h1>
                        <h2>Fotografo</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DANIELE">

                </div>
            </div>
        </a>
    </div>

    <div class="red-back"></div>

    <div class="team-popup-background" id="team_popup" style="display: none;">
        <div class="team-popup-container">
            <div class="col-container">
                <div class="col col-40">
                    <img src="images/team/x/ANDREA.PNG" width="100%" id="team_popup_image" />
                </div>
                <div class="col col-60">
                    <div class="team-popup-info">
                        <div>
                            <h1 id="team_popup_title">Andrea Franchin</h1>
                            <h2 id="team_popup_subtitle">Licenziatario e organizzatore</h2>
                            <p id="team_popup_description">Ogni scarrafone è bello a mamma sua. La gatta cieca ci ha lasciato lo zampino e i topini che erano  con lei hanno imparato a zoppicare e ora sto cazzo che loro ballano quando lei non c’è. </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="team-popup-left">
                <a href="#" id="team_popup_previous"></a>
            </div>

            <div class="team-popup-right">
                <a href="#" id="team_popup_next"></a>
            </div>

            <div class="team-popup-close">
                <a href="javascript: hidePopup();"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>
</div>
<script>
    var team = [
        {
            id: "ANDREA",
            name: "Andrea Franchin",
            role: "Licenziatario e organizzatore",
            description: "Se il cinema è il mondo dei sogni, lui è un vero sognatore. I suoi miti sono George Lucas, Howard Hughes e Dino De Laurentiis. È volato a Los Angeles per studiare cinema, e ora che è di nuovo in Italia continua a inseguire quel sogno dietro la macchina da presa, producendo film, video e documentari."
        },
        {
            id: "EMILIANO",
            name: "Emiliano Guerra",
            role: "Coordinatore partner",
            description: "Geometra con l’hobby della fotografia e la passione per tutto ciò che vola, è uno dei promotori di TEDx Montebelluna e fa parte del team partner. È un moderno Moschettiere: crede nelle alleanze strette per il bene comune, è attivo nel volontariato e attento alla salvaguardia del territorio e dell’ambiente. Il suo motto non poteva che essere “tutti per uno, uno per tutti!”."
        },
        {
            id: "DARIO",
            name: "Dario Merlo",
            role: "Art director",
            description: "Grafico, illustratore e Art director di TEDx Montebelluna. Crede negli eventi TED perché per lui le idee sono i semi da cui nascono i grandi cambiamenti.<br /><br />Se scuoti l'albero delle idee non sai mai quale frutto cadrà. Ti potrà colpire una mela in testa e farti scoprire la gravità, oppure potrai morderla e creare scompiglio nel Giardino dell’Eden, o sceglierla come logo di un’azienda innovativa che farà la storia. Di sicuro sarà una rivoluzione. "
        },
        {
            id: "BEATRICE",
            name: "Beatrice Pesente",
            role: "Copywriter e Social media manager",
            description: "Copywriter e social media manager, si occupa di visionare tutte le parole scritte di TEDx Montebelluna (comprese queste). Non ha smesso un attimo di scrivere da quando ha imparato a tenere in mano una penna (a un certo punto l’ha solo sostituita con la tastiera). Non toccatele i Beatles, Stranger Things o Star Wars: potrebbe diventare irascibile."
        },
        {
            id: "ALBERTO",
            name: "Alberto Moretto",
            role: "Comunicazione",
            description: "Studia Ingegneria informatica e per TEDx fa parte del team comunicazione. È un ingegnere con un’anima sportiva. Ama allo stesso modo la tecnologia, il calcio e il basket. Le uniche cose a cui non arriva mai in ritardo sono le partite del Milan e quelle dei Celtics. Per il resto, la sua vita è un vero elogio della lentezza. "
        },
        {
            id: "ELENA",
            name: "Elena Bressan",
            role: "Coordinatore speaker",
            description: "Laureata in marketing, lavora in Olanda per un’azienda di orologi e si occupa di Influencer Marketing. Fa parte del team speaker di TEDx. È l’italiana meno italiana del gruppo: alterna Starbucks al caffè della moka, il sashimi alla pizza. Ama viaggiare, e nella sua top 3 dei viaggi da sogno ci sono l’Islanda, le risaie a Bali e il Myanmar. Ma se volete partire con lei, portatevi dietro una bussola: ha un pessimo senso dell’orientamento."
        },
        {
            id: "ENRICO",
            name: "Enrico Poloni",
            role: "Coordinatore partner",
            description: "Progettista elettronico e consulente energetico, è parte anche lui del team partner. Studioso per passione di crescita personale, conosce tecniche di lettura veloce, memorizzazione strategica e meditazione. Ha un animo artistico e in passato ha provato anche a dipingere. Forse la sua prossima sfida sarà provare a cantare: avete notato quanto assomiglia ad Alvaro Soler? "
        },
        {
            id: "ERIKA",
            name: "Erika Martinazzo",
            role: "Coordinatore speaker",
            description: "Lavora in un’azienda di moda come traduttrice e nell’area business innovation. Fa parte del team speaker TEDx. Gli studi in lingue l’hanno avvicinata a culture diverse e vive il suo lavoro come la costruzione di un ponte che unisce le persone e i Paesi. Ma non è mica finita qui: partecipa a contest di traduzione, scrive, disegna, ha in cantiere progetti di musica e grafica. Forse le sue giornate durano più delle nostre?"
        },
        {
            id: "FABIO",
            name: "Fabio Donà",
            role: "Coordinatore speaker",
            description: "Consulente commerciale e marketing, per TEDx Montebelluna si occupa di accompagnare e formare gli speaker. La fase dei “perché” per lui non è finita dopo l’infanzia: se da piccolo smontava i giocattoli per capire come funzionavano, da grande continua a farsi domande sui meccanismi che regolano le relazioni umane. Fervido sostenitore dell’effetto farfalla, sa che ogni scelta ha conseguenze molto più profonde di quello che ci si può immaginare."
        },
        {
            id: "FEDERICA",
            name: "Federica Favero",
            role: "Responsabile allestimenti",
            description: "Si occupa di eventi e declina questa sua attitudine organizzativa anche nel team comunicazione di TEDx Montebelluna. Non le piace proprio stare con i piedi per terra! Entusiasmo e curiosità la spingono a volare con l’immaginazione inseguendo l’orbita delle idee generate dall'ingegno e dalla creatività umana, o a volare per davvero per scoprire nuove curiosità del mondo. "
        },
        {
            id: "FRANCESCA",
            name: "Francesca Celato",
            role: "Coordinatore speaker",
            description: "Laureata in ingegneria edile architettura, per TEDx Montebelluna fa parte del team speaker. Vive di opposti: la sua anima analitica si armonizza con uno spirito creativo, è precisa ma con la testa tra le nuvole, rigorosa ma sognatrice. Quando la sua parte creativa prende il sopravvento rischia di perdere le chiavi di casa, ma riesce a trovare grandi idee! "
        },
        {
            id: "FRANCESCO",
            name: "Francesco Calzamatta",
            role: "Comunicazione",
            description: "Laureato in Design della comunicazione, per TEDx fa parte del team comunicazione. Ama il rock Anni Ottanta e Novanta, e se volete farlo felice portatelo a un concerto. Quale? Uno qualsiasi, basta che sul palco ci sia qualcuno che ci dà dentro con la chitarra (vi dicono niente gli AC/DC?). Ah, è meglio se lo accompagnate voi in macchina, perché si dimentica sempre dove ha parcheggiato."
        },
        {
            id: "GIANLUCA",
            name: "Gianluca Innocente",
            role: "Web developer",
            description: "Diplomato in Informatica, è il programmatore di TEDx Montebelluna. Siti web e app mobile non hanno segreti per lui. L’unico momento in cui non affronta le righe di codice o studia le nuove tecnologie nel campo della programmazione, è quando gioca a calcetto e si trova di fronte la porta dell’avversario. "
        },
        {
            id: "MYRNA",
            name: "Myrna Isetta",
            role: "Responsabile traduzioni",
            description: "Commerciale estero in un’azienda vinicola, per TEDx si occupa delle traduzioni. Nessuno ama il cinema più di lei. Resta sveglia fino alle sei di mattina per vedere la cerimonia degli Oscar, e tifa sempre per Meryl Streep (anche quando non è tra i nominati). Le sue armi? Il pungente sarcasmo e un perfetto American English."
        },
        {
            id: "DANIELE",
            name: "Daniele Bozzano",
            role: "Fotografo",
            description: "Genovese trapiantato da otto anni in Veneto, è il fotografo di TEDx Montebelluna.<br />Crescere vicino a un porto di arrivi e partenze lo ha fatto diventare un viaggiatore. Ha il mare nell’animo, ma alla nave preferisce la motocicletta.<br />Ha vissuto a Modena, a Valencia e a Milano, dove ha studiato fotografia. Da allora non ha mai smesso di fotografare, seguendo quello che diceva Henri Cartier-Bresson: “la fotografia può raggiungere l’eternità attraverso il momento”."
        }
    ];

    $(document).ready(function() {
        $(".team-hover").hover(function() {
            var element = $("#hover_content_" + this.id);
            var display = element.css("display");

            console.log("display "+display+"  for id "+this.id);

            if(display == 'none') {
                $("#image_"+this.id).attr("src", "images/team/colors/" + this.id + ".jpg");
                element.show();
                console.log("show "+this.id);
            }
        }, function() {
            var display = this.style.display;

            console.log("hide "+display+"  id "+this.id);

            $("#image_"+this.id).attr("src", "images/team/black/" + this.id + ".jpg");
            $("#hover_content_" + this.id).hide();
        });

        $(".team-hover").click(function() {
            var itemID = this.id;
            var element = this;
            var userID = itemID.replace("link_", "");

            show(userID);

                var topTarget = $(element).offset().top;

                console.log("Before Footer y:"+$("footer").offset().top+"   topTarget:"+topTarget+"    heightPopup:"+$(".team-popup-container").height());

                if($("footer").offset().top < topTarget + $(".team-popup-container").height() + 290) {
                    topTarget = 120;
                }

                $('html, body').animate({
                                scrollTop: topTarget
                            }, 500);

                $("#team_popup").css("top", (topTarget - 90) + "px");

        });
    });

    function show(userID) {
        $("#team_popup").fadeOut(200, function() {
            var userIndex = getUserIndexById(userID);
            var user = team[userIndex];
            var prevUser = userIndex > 0 ? team[userIndex - 1] : team[team.length - 1];
            var nextUser = userIndex < team.length - 1 ? team[userIndex + 1] : team[0];

            $("#team_popup_image").attr("src", "");
            $("#team_popup_image").attr("src", "images/team/x/" + userID + ".png");
            $("#team_popup_title").html(user.name);
            $("#team_popup_subtitle").html(user.role);
            $("#team_popup_description").html(user.description);

            $("#team_popup_previous").html("< " + prevUser.id);
            $("#team_popup_previous").attr("href", "javascript: show('" + prevUser.id + "');");
            $("#team_popup_next").html(nextUser.id + " >");
            $("#team_popup_next").attr("href", "javascript: show('" + nextUser.id + "');");

            $("#team_popup").fadeIn(200);

            $(".red-back").fadeIn();
        });
    }

    function getUserIndexById(userID) {
        var usrIndex = null;

        for(var i = 0; i < team.length; i++) {
            if(team[i].id == userID) {
                usrIndex = i;
            }
        }

        return usrIndex;
    }
</script>
