<style>
footer {
    border-top: none;
}

.team-popup-info p {
    line-height: 30px;
}

.team-popup-container {
    padding-top: 0px;
    top: 30px;
}


</style>
<div class="row" style="position: relative; margin: 0;">

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_DURANTE"><img src="images/speaker/red/DURANTE.jpg" width="100%" id="image_DURANTE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DURANTE">
                    <div class="team-fast-hover">
                        <h1>Daniele Durante</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DURANTE">

                </div>
            </div>
        </a>
    </div>
    
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_WEYLAND"><img src="images/speaker/red/WEYLAND.jpg" width="100%" id="image_WEYLAND" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_WEYLAND">
                    <div class="team-fast-hover">
                        <h1>Beate Weyland</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="WEYLAND">

                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_PROVEDEL"><img src="images/speaker/red/PROVEDEL.jpg" width="100%" id="image_PROVEDEL" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_PROVEDEL">
                    <div class="team-fast-hover">
                        <h1>Renzo Provedel</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="PROVEDEL">

                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_MARCOLIN"><img src="images/speaker/red/MARCOLIN.jpg" width="100%" id="image_MARCOLIN" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_MARCOLIN">
                    <div class="team-fast-hover">
                        <h1>Luca Marcolin</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="MARCOLIN">

                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_GHENO"><img src="images/speaker/red/GHENO.jpg" width="100%" id="image_GHENO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_GHENO">
                    <div class="team-fast-hover">
                        <h1>Vera Gheno</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="GHENO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_DECARLO"><img src="images/speaker/red/DECARLO.jpg" width="100%" id="image_DECARLO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DECARLO">
                    <div class="team-fast-hover">
                        <h1>Giorgio De Carlo</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DECARLO">

                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_MAREGA"><img src="images/speaker/red/MAREGA.jpg" width="100%" id="image_MAREGA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_MAREGA">
                    <div class="team-fast-hover">
                        <h1>Antonello Marega</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="MAREGA">

                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_VISENTIN"><img src="images/speaker/red/VISENTIN.jpg" width="100%" id="image_VISENTIN" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_VISENTIN">
                    <div class="team-fast-hover">
                        <h1>Andrea Visentin</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="VISENTIN">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_SUMINI"><img src="images/speaker/red/SUMINI.jpg" width="100%" id="image_SUMINI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_SUMINI">
                    <div class="team-fast-hover">
                        <h1>Valentina Sumini</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="SUMINI">

                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_BARCO"><img src="images/speaker/red/BARCO.jpg" width="100%" id="image_BARCO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_BARCO">
                    <div class="team-fast-hover">
                        <h1>Davide Barco</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="BARCO">

                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_BELLAVISTA"><img src="images/speaker/red/BELLAVISTA.jpg" width="100%" id="image_BELLAVISTA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_BELLAVISTA">
                    <div class="team-fast-hover">
                        <h1>Massimo Soriani Bellavista</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="BELLAVISTA">

                </div>
            </div>
        </a>
    </div>
    
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="#" id="link_BONACCINI"><img src="images/speaker/red/BONACCINI.jpg" width="100%" id="image_BONACCINI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_BONACCINI">
                    <div class="team-fast-hover">
                        <h1>Filippo Bonaccini</h1>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="BONACCINI">

                </div>
            </div>
        </a>
    </div>

    <div class="red-back"></div>

    <div class="team-popup-background" id="team_popup" style="display: none;">
        <div class="team-popup-container">
            <div class="col-container">
                <div class="col col-40">
                    <img src="images/speaker/col/DANIELE.jpg" width="100%" id="team_popup_image" />
                </div>
                <div class="col col-60" style="padding: 25px;">
                    <div class="team-popup-info">
                        <div>
                            <h1 id="team_popup_title">Daniele Durante</h1>
                            <p id="team_popup_description"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="team-popup-left">
                <a href="#" id="team_popup_previous"></a>
            </div>

            <div class="team-popup-right">
                <a href="#" id="team_popup_next"></a>
            </div>

            <div class="team-popup-close">
                <a href="javascript: hidePopup();"><i class="fa fa-times"></i></a>
            </div>
        </div>
    </div>
</div>
<script>
    var team = [
        {
            id: "DURANTE",
            name: "Daniele Durante",
            role: "",
            description: "Daniele Durante è un giovane Professore di Statistica presso il Dipartimento di Scienze delle Decisioni dell’Università Bocconi di Milano. Ha studiato Scienze Statistiche tra l’Università di Padova e la Duke University negli Stati Uniti. Si definisce uno “scienziato dei dati” e ama la natura, soprattutto quando si esprime tramite numeri e linguaggi matematici. <br /><br />A TEDx Montebelluna ci spiegherà la geometria nascosta delle relazioni che instauriamo ogni giorno, proiettandoci in nuovi spazi matematici dove le sequenze di connessioni tra persone che ci dividono influenzano la nostra vita e le nostre decisioni ben oltre le distanze fisiche."
        },
        {
            id: "WEYLAND",
            name: "Beate Weyland",
            role: "",
            description: "Beate Weyland è professore associato di didattica presso la Facoltà di Scienze della Formazione della Libera Università di Bolzano. <br />La sua ricerca approfondisce il rapporto tra pedagogia, architettura e design nella trasformazione e sviluppo della scuola, ed esplora la qualità della progettazione condivisa. <br />Opera all’interno del collettivo PULS e PAD, un gruppo di pedagogisti, architetti e designer.  Il collettivo promuove studi, mostre e convegni a livello internazionale per sensibilizzare sul rapporto tra spazi e didattiche e per dare informazioni pedagogiche al corpo materiale della scuola.<br /><br />Nella sua talk “Progettare scuole insieme tra pedagogia, architettura e design” ci parlerà degli spazi scolastici che sono, da una parte, specchi di un modo di pensare e lavorare, e dall’altra potenti dispositivi pedagogici. Ci mostrerà una possibile via per progettare i luoghi della cultura del sapere come spazi di benessere per tutti."
        },
        {
            id: "PROVEDEL",
            name: "Renzo Provedel",
            role: "",
            description: "Renzo Provedel è un coach per le strategie e l’innovazione e ha partecipato allo sviluppo della prima scuola di coaching in Italia (SCOA, a Milano).  La sua esperienza come manager si è svolta in ambito ICT (Information and Communication technology) e in Logistica, competenze che ora, da imprenditore, utilizza per facilitare l’innovazione delle grandi imprese, ed è focalizzato sulll’Open Innovation. Oggi è impegnato a lanciare una startup innovativa, BRIT, per il ri-uso di edifici storici. <br /><br />Nel suo intervento a TEDx Montebelluna spiegherà cosa significa “innovazione aperta”e come si possono trovare nuove soluzioni ai problemi interni a un’azienda o a un settore utilizzando risorse esterne e apparentemente estranee."
        },
        {
            id: "MARCOLIN",
            name: "Luca Marcolin",
            role: "",
            description: "Luca Marcolin è counselor, coach e trainer di programmazione neuro-linguistica.<br />Dopo la Laurea in Economia Aziendale si è occupato di risorse umane, di organizzazione e controllo di gestione per grandi aziende.<br />Ha poi sviluppato la sua società di consulenza, The Family Business Unit, e si è dedicato sempre di più alle imprese di famiglia, accompagnando gli imprenditori e il management nella loro crescita.<br />Tiene lezioni di management e di family business presso l'Università di Padova e la Business School CUOA.<br />A TEDx Montebelluna ci parlerà di imprese familiari e della loro sfida nel combinare armonia e risultati. Ci racconterà cosa ha imparato dalle famiglie imprenditoriali che può essere utili per vivere le nostre famiglie e per gestire le nostre imprese."
        },
        {
            id: "GHENO",
            name: "Vera Gheno",
            role: "",
            description: "Vera Gheno è una sociolinguista e si occupa da vent'anni di comunicazione mediata dal computer. Insegna all'università di Firenze e tiene corsi sull'etica della comunicazione in scuole, aziende e master universitari.<br />Gestisce il profilo Twitter dell'Accademia della Crusca e collabora con Zanichelli per questioni di lingua e di educazione ai nuovi media. Traduce letteratura dall’ungherese all’italiano. <br />È autrice di due libri: “Guida pratica all’italiano scritto (senza diventare grammarnazi)”e “Social-linguistica. Italiano e italiani dei social network”.<br /><br />A TEDx Montebelluna ci farà riflettere su come siamo gli unici esseri viventi a possedere la capacità del linguaggio, ma spesso non ci rendiamo conto di questa capacità. Con le parole rappresentiamo la realtà e con le parole possiamo accarezzare e ferire, convincere e respingere. Ci spingerà ad amare di nuovo la nostra lingua e a riscoprire il potere di usare le parole corrette."
        },
        {
            id: "DECARLO",
            name: "Giorgio De Carlo",
            role: "",
            description: "Giorgio De Carlo è docente al Master universitario in Strategie per il Business dello Sport dell’Università Ca’ Foscari di Venezia.<br />È fondatore e direttore dell’Istituto di ricerca Queris, leader per ricerche di mercato, indagini di marketing strategico e sondaggi d’opinione.<br />Ha collaborato come ricercatore e consulente con il Ministero dell'Istruzione dell'Università e della Ricerca, è stato membro della commissione RAI per il portale RAI Educational e fa parte del gruppo Atlantide.<br />Nel suo intervento a TEDx Montebelluna parlerà del Prediction Market, un gioco di scommesse competitivo, studiato per sfruttare la capacità dell'intelligenza collettiva di prevedere il futuro.<br />È una tecnica basata sul ruolo dell’intelligenza della folla nel fare previsioni negli ambiti più svariati: il successo di una nuova idea, l’andamento di un nuovo prodotto, il risultato di una competizione elettorale e molto altro.",
        },
        {
            id: "MAREGA",
            name: "Antonello Marega",
            role: "",
            description: "Antonello Marega è Presidente di EPSI, una piattaforma europea per l'innovazione nello sport. È anche Professore allo IUAV di Venezia nel corso di Design di prodotto e strategia d'impresa.È stato responsabile del reparto Ricerca e Sviluppo per Tecnica S.p.a., e si è occupato del lancio di diversi prodotti nel settore della calzatura sportiva e per il tempo libero, seguendone l'ideazione, lo sviluppo del design, i test e la produzione.<br /><br />Per TEDx Montebelluna ci parlerà di Circular Economy, o economia circolare, basata sul concetto che gli scarti siano risorse da riutilizzare, e ci racconterà le città del futuro, sempre più smart ma abitate da persone sempre più anziane, spesso con grossi problemi di obesità. Insieme a lui scopriremo anche quali sono le attività motorie consigliate per questo nuovo tipo di popolazione.",
        },
        {
            id: "VISENTIN",
            name: "Andrea Visentin",
            role: "",
            description: "Andrea Visentin è un Ingegnere informatico ed è partner di un’azienda che si occupa di sviluppo web e di piattaforme di e-learning per farmacie e laboratori farmaceutici. Sta completando un Dottorato di Ricerca in Intelligenza Artificiale applicata all’industria all’Università di Cork, in Irlanda. <br /><br />Per TEDx Montebelluna parlerà proprio di Intelligenza Artificiale, un termine che vediamo spesso nell’attualità e nei film di fantascienza. Ma quanto è vicina questa rappresentazione a quello che l’Intelligenza Artificiale può davvero fare per noi? In questo speech verranno affrontati temi come il Decision Making e l’industria 4.0 e il modo in cui l’uso dei computer sta cambiando il mondo del lavoro. "
        },
        {
            id: "SUMINI",
            name: "Valentina Sumini",
            role: "",
            description: "Valentina Sumini è Postdoctoral Associate al Massachusetts Institute of Technology di Boston e collabora con diversi gruppi di ricerca: Digital Structures del Dipartimento di Ingegneria civile e ambientale, Tangible Media e Space Exploration Initiative del MIT MediaLab.<br />Nella sua attività di ricerca esplora strategie di ottimizzazione per la ricerca di forme architettoniche per gli habitat di esplorazione spaziale sulla Luna e Marte. La ricerca è applicata a tre diversi scenari: una città su Marte, un villaggio lunare e un hotel di lusso orbitante intorno alla Terra.<br />A TEDx Montebelluna ci farà viaggiare in un futuro non troppo lontano in cui l’uomo comincerà ad abitare su Marte e avrà bisogno di infrastrutture progettate per superare le sfide dell’ambiente extra-planetario.<br />Ci farà conoscere la figura dell’architetto spaziale e capiremo quali sono le dinamiche e gli ostacoli che deve affrontare chi progetta per Marte e come queste soluzioni architettoniche potrebbero aiutarci a raggiungere un futuro più sostenibile anche sulla Terra.",
        },
        {
            id: "BARCO",
            name: "Davide Barco",
            role: "",
            description: "Davide Barco è un illustratore padovano appassionato di sport. Ha studiato allo IED e ha lavorato come Art director per agenzie pubblicitarie internazionali. Vive e lavora a Milano e ha illustrato, tra gli altri, per NBA, HBO Europe, il New York Times, il Wall Street Journal, Milan, Juventus e American Express.<br /><br />Nella sua divertente talk “Elogio alla sfortuna” ripercorrerà con ironia il suo percorso umano e professionale, di come è diventato un illustratore sbagliando molto come Art director, tra New York, Londra e l’Italia."
        },
        {
            id: "BELLAVISTA",
            name: "Massimo Soriani Bellavista",
            role: "",
            description: "Massimo Soriani Bellavista collabora da vent’anni con Edward De Bono, l’inventore del pensiero laterale di cui è Master Trainer e rappresentante ufficiale in Italia.<br />Laureato in psicologia del lavoro, negli anni ha integrato vari modelli di business tra cui una metodologia chiamata Markethink che integra pensiero laterale, brainstorming e marketing. <br />Da dodici anni collabora con la scuola Universitaria Supsi in Svizzera per le aree di management, risorse umane e creatività. Ha fondato due società di formazione e consulenza che si occupano di formazione aziendale e digital innovation. <br /><br />Nel suo speech a TEDx Montebelluna ci porterà in un viaggio nel pensiero laterale e ci darà degli stimoli su come canalizzare le buone idee attraverso la rete e trasformarle in progetti creativi."
        },
        {
            id: "BONACCINI",
            name: "Filippo Bonaccini",
            role: "",
            description: "Filippo Bonaccini è un ingegnere civile laureto all’Università di Padova e alla University of California di Berkeley.<br /><br />Prima progettista strutturale presso Italstrade, dal 1992 esercita la professione a Treviso. <br />Dopo un periodo in cui si è dedicato alla progettazione strutturale, estende l’attività professionale alla progettazione generale, sia per il settore pubblico che per il privato.<br /><br />Nel 1998 diventa socio fondatore e direttore tecnico di Opera Engineering, che nel 2012 si evolve in Urban Professionals.<br /><br />Per Urban Professionals si occupa di ingegneria energetica, e ha acquisito la certificazione ICMQ come EGE (Esperto nella gestione dell’energia). A TEDx Montebelluna ci parlerà di Smart District, un modello di re-urbanizzazione sostenibile dei quartieri attraverso la costruzione di modelli innovativi di abitazione e di gestione dell’energia."
        },
    ];

    $(document).ready(function() {
        $(".team-hover").hover(function() {
            var element = $("#hover_content_" + this.id);
            var display = element.css("display");

            console.log("display "+display+"  for id "+this.id);

            if(display == 'none') {
                $("#image_"+this.id).attr("src", "images/speaker/col/" + this.id + ".jpg");
                element.show();
                console.log("show "+this.id);
            }
        }, function() {
            var display = this.style.display;

            console.log("hide "+display+"  id "+this.id);

            $("#image_"+this.id).attr("src", "images/speaker/red/" + this.id + ".jpg");
            $("#hover_content_" + this.id).hide();
        });

        $(".team-hover").click(function() {
            var itemID = this.id;
            var element = this;
            var userID = itemID.replace("link_", "");

            show(userID);

                var topTarget = $(element).offset().top;

                console.log("Before Footer y:"+$("footer").offset().top+"   topTarget:"+topTarget+"    heightPopup:"+$(".team-popup-container").height());

                if($("footer").offset().top < topTarget + $(".team-popup-container").height() + 90) {
                    topTarget = 90;
                }

                $('html, body').animate({
                                scrollTop: topTarget
                            }, 500);

                $("#team_popup").css("top", (topTarget - 90) + "px");

        });
    });

    function show(userID) {
        $("#team_popup").fadeOut(200, function() {
            var userIndex = getUserIndexById(userID);
            var user = team[userIndex];
            var prevUser = userIndex > 0 ? team[userIndex - 1] : team[team.length - 1];
            var nextUser = userIndex < team.length - 1 ? team[userIndex + 1] : team[0];

            $("#team_popup_image").attr("src", "");
            $("#team_popup_image").attr("src", "images/speaker/col/" + userID + ".jpg");
            $("#team_popup_title").html(user.name);
            $("#team_popup_subtitle").html(user.role);
            $("#team_popup_description").html(user.description);

            $("#team_popup_previous").html("< " + prevUser.id);
            $("#team_popup_previous").attr("href", "javascript: show('" + prevUser.id + "');");
            $("#team_popup_next").html(nextUser.id + " >");
            $("#team_popup_next").attr("href", "javascript: show('" + nextUser.id + "');");

            $("#team_popup").fadeIn(200);

            $(".red-back").fadeIn();
        });
    }

    function getUserIndexById(userID) {
        var usrIndex = null;

        for(var i = 0; i < team.length; i++) {
            if(team[i].id == userID) {
                usrIndex = i;
            }
        }

        return usrIndex;
    }
</script>
