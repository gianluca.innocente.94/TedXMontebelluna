<div class="ted-header-image">
            <!--<img src="images/header_image.png" width="100%" />-->
    <video playsinline autoplay muted loop poster="video/home.jpg" id="bgvid" width="100%">
        <source src="video/home.webm" type="video/webm">
        <source src="video/home.mp4" type="video/mp4">
    </video>

    <div class="video-date">
        <div class="table-cont">
            <div class="table-cell">
                <div style="display: inline-block">RIVIVI L'EVENTO<br /><a href="talk"><button class="btn btn-primary btn-lg">Guarda le talk</button></a></div>
            </div>
        </div>
    </div>

            <div class="ted-header-image-arrow">
                <a href="javascript: goDown();">
                    <img src="images/header_arrow.png" />
                </a>
            </div>
        </div>

        <div class="container ted-symbol-box" id="go_down">
            <a href="gallery"><button class="btn-buy only-mobile">VEDI LA GALLERY</button></a>

            <div class="row">
                <div class="col-md-5 ted-symbol-box-left">
                    <img src="images/tedx_symbol.png" style="max-width: 90%" />
                </div>
                <div class="col-md-7 ted-symbol-box-right">
                    <h1>Idee in Orbita</h1>
                    <p>Il motto degli eventi TED è "idee che meritano di essere diffuse" e noi di TedxMontebelluna le lanceremo addirittura nello spazio!<br /><br />Il titolo dell'evento 2018 è: <b>"Idee in orbita"</b>.</p>
                </div>
            </div>
        </div>

        <div class="ted-grid">
            <div class="row" style="max-width: 100%; margin: 0 auto;">
                <div class="col-sm-4">
                    <a href="speaker">
                        <div class="row">
                            <div class="col-sm-6 no-padding">
                                <div class="quest-cont home-grid-padding home-grid-base  home-grid-row">
                                    <div class="table-cont">
                                        <div class="table-cell">
                                            <!--<img src="images/gif-speak.gif" style="max-width: 100%" />-->
                                        </div>
                                    </div>
                                    <div class="quest-arrow"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padding">
                                <div class="speaker-cont home-grid-padding home-grid-base  home-grid-row">
                                    <div class="table-cont">
                                        <div class="table-cell">
                                            Gli speaker<br />dell'evento
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="row">
                        <div class="col-sm-12 no-padding">
                            <div class="ted-cont home-grid-base  home-grid-row">
                                <div class="table-cont">
                                    <div class="table-cell">
                                        <img src="images/ted.png" />
                                    </div>
                                </div>
                                <div class="ted-arrow"></div>
                            </div>
                        </div>
                        <div class="col-sm-12 no-padding">
                            <div class="creat-cont home-grid-padding home-grid-base  home-grid-row">
                                <div class="table-cont">
                                    <div class="table-cell">
                                        Le conferenze TED:<br />idee che meritano di essere diffuse
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12 no-padding">
                            <div class="infinite-cont home-grid-base  home-grid-row">
                                <div class="infinite-arrow"></div>
                                <div class="table-cont" style="background: rgba(0,0,0,0.5);">
                                    <div class="table-cell">
                                        <a href="https://www.infinitearea.com" target="_blank"><img src="images/partner/tecnici/infinite_area.png" width="190" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 no-padding">
                            <div class="location-cont home-grid-padding home-grid-base home-grid-row">
                                <div class="table-cont">
                                    <div class="table-cell">
                                        Ogni idea ha bisogno di un luogo:<br />scopri Infinite Area
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="team">
                            <div class="col-sm-6 no-padding">
                                <div class="team-cont home-grid-padding home-grid-base home-grid-row">
                                    <div class="table-cont">
                                        <div class="table-cell">
                                            Vieni a conoscere<br />il nostro team
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padding">
                                <div class="home-grid-base home-grid-row no-padding">
                                    <div class="member-cont home-grid-row">
                                        <div class="team-arrow"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <a href="javascript: showModal('teaser_modal');">
                            <div class="col-sm-6 no-padding">
                                <div class="innovation-cont home-grid-padding home-grid-base home-grid-row">
                                    <div class="table-cont">
                                        <div class="table-cell">
                                            Le nostre idee<br />in orbita
                                        </div>
                                    </div>
                                </div>
                                <div class="trump-cont home-grid-double-row">
                                <div class="trump-arrow"></div>
                                </div>
                            </div>
                        </a>
                        <div class="col-sm-6 no-padding">
                            <div class="home-grid-base  home-grid-row no-padding">
                                <div class="cat-cont home-grid-row">
                                    <div class="cat-arrow"></div>
                                </div>
                            </div>
                            <div class="creat-cont home-grid-padding home-grid-base  home-grid-row">
                                <div class="table-cont">
                                    <div class="table-cell">
                                        Innovazione<br />e creatività
                                    </div>
                                </div>
                            </div>
                            <div class="facebook-cont home-grid-padding home-grid-base  home-grid-row">
                                <div class="table-cont">
                                    <div class="table-cell">
                                        <a href="https://www.facebook.com/tedxmontebelluna/" target="_blank"><img src="images/facebook.png" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ted-program">
            <div class="row program-title-row">
                <div class="col-sm-12">
                    <h1>Programma</h1>
                </div>
            </div>

            <table>
                <tr>
                    <td><div class="program-time">10.00 - 10.45</div></td>
                    <td class="program-arrow">-</td>
                    <td class="program-item">Ingresso check-in</td>
                </tr>
                <tr>
                    <td><div class="program-time">11.00 - 12.45</div></td>
                    <td class="program-arrow">-</td>
                    <td class="program-item">Prima sessione talk</td>
                </tr>
                <tr>
                    <td><div class="program-time">13.00 - 14.00</div></td>
                    <td class="program-arrow">-</td>
                    <td class="program-item">Pranzo a buffet</td>
                </tr>
                <tr>
                    <td><div class="program-time">14.15 - 15.45</div></td>
                    <td class="program-arrow">-</td>
                    <td class="program-item">Seconda sessione talk</td>
                </tr>
                <tr>
                    <td><div class="program-time">15.45 - 16.30</div></td>
                    <td class="program-arrow">-</td>
                    <td class="program-item">Pausa caffè</td>
                </tr>
                <tr>
                    <td><div class="program-time">16.30 - 18.00</div></td>
                    <td class="program-arrow">-</td>
                    <td class="program-item">Terza sessione talk</td>
                </tr>
                <tr>
                    <td><div class="program-time">18.00 - 19.00</div></td>
                    <td class="program-arrow">-</td>
                    <td class="program-item">Aperitivo finale</td>
                </tr>

            </table>
            <p>Durante la fase di check-in sarà necessario esibire<br />il biglietto cartaceo o digitale per accedere all’evento.</p>
            <br /><br /><br />
        </div>

<?php
include("sponsor_modal.php");
include("teaser_modal.php");
?>

<script>
    $(document).ready(function() {
        $("#sponsor_form").submit(function() {
            $("#sponsor_message").fadeOut();

            var params = {
                name: $("#sponsor_name").val(),
                surname: $("#sponsor_surname").val(),
                email: $("#sponsor_email").val(),
                phone: $("#sponsor_phone").val(),
                notes: $("#sponsor_notes").val(),
                captcha_response: $("#g-recaptcha-response").val()
            };

            $.post("core/sponsor.php", params, function (data) {
                $("#sponsor_message").html(data);
                $("#sponsor_message").fadeIn();

                return false;
            }).fail(function (error) {
                $("#sponsor_message").html("Si è verificato un errore imprevisto. Ti preghiamo di contattarci scrivendo a info@tedxmontebelluna.com");

                $("#sponsor_message").fadeIn();
            });

            return false;
        });

        $("#teaser_modal").on('hidden.bs.modal', function (e) {
            $("#teaser_modal iframe").attr("src", $("#teaser_modal iframe").attr("src"));
        });
    });

    function goDown() {
        $('html, body').animate({
                            scrollTop: $("#go_down").offset().top
                        }, 500);
    }
</script>