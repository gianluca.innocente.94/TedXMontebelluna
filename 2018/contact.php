<style>
.page {
    background: #131313;
}
</style>
<br /><br />
<div class="container">
    <div class="row">
        <div class="col-sm-6 contacts-desc">
            <div style="height: 355px; display: table;">
                <div style="display: table-cell; vertical-align: middle; text-align: center;">
                    Scrivici un messaggio
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <form role="form" id="contact_form">
                    <div class="modal-body">
                        <div class="box-body row">
                            <div class="col-sm-6">
                                <br />
                                <div>
                                    <label for="contact_name">Nome*</label>
                                    <input type="text" class="form-control" id="contact_name" required>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <br />
                                <div>
                                    <label for="contact_surname">Cognome*</label>
                                    <input type="text" class="form-control" id="contact_surname" required>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <br />
                                <div>
                                    <label for="contact_email">E-mail*</label>
                                    <input type="text" class="form-control" id="contact_email" required>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <br />
                                <div>
                                    <label for="contact_msg">Messaggio</label>
                                    <textarea class="form-control" id="contact_msg"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <br />
                                <div>
                                    <div class="g-recaptcha" data-sitekey="6LfWq1EUAAAAAHHeSpddiTlCJmMzStQ9uBfqJz7a"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Invia</button>
                        <br /><br />
                        <div class="modal-message" id="contact_message" style="display: none"></div>
                    </div>
                </form>
        </div>
    </div>
</div>
<br /><br />
<div class="print-office">
<div class="container">
    <div class="row">
        <div class="col-sm-6 contacts-desc">
            <div style="height: 255px; display: table;">
                <div style="display: table-cell; vertical-align: middle; text-align: center;">
                    Ufficio stampa
                </div>
            </div>
        </div>
        <div class="col-sm-6">
        <br /><br />
        <div style="height: 255px; display: table;">
                <div style="display: table-cell; vertical-align: middle; text-align: left; font-size: 20px;">
        <p>Silvia Milani<br />
e-mail: ufficiostampa@milanipress.it<br />
Tel.: +39 348 2562694</p><br /><br />
</div>
</div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#contact_form").submit(function() {
            $("#contact_message").fadeOut();

            var params = {
                name: $("#contact_name").val(),
                surname: $("#contact_surname").val(),
                email: $("#contact_email").val(),
                msg: $("#contact_msg").val(),
                captcha_response: $("#g-recaptcha-response").val()
            };

            $.post("core/contact.php", params, function (data) {
                $("#contact_message").html(data);
                $("#contact_message").fadeIn();

                return false;
            }).fail(function (error) {
                $("#contact_message").html("Si è verificato un errore imprevisto. Ti preghiamo di contattarci scrivendo a info@tedxmontebelluna.com");

                $("#contact_message").fadeIn();
            });

            return false;
        });
    });
</script>
