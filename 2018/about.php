<style>
.page {
    background: #131313;
}
</style>
<div class="container">
    <br /><br />
    <div class="row">
        <div class="col-sm-5">
            <img src="images/ideas_worth.png" width="90%" />
        </div>
        <div class="col-sm-7 about-box">
            <br /><br /><br /><br /><br />
            <h1>IL TED</h1>
            <h2>Ideas worth spreading</h2>
            <br />
            <p>Il <b>TED</b> (acronimo per <b>Technology, Entertainment, Design</b>) è un&#39;organizzazione noproft che mette
in piedi eventi con l’obiettivo di far incontrare le idee più stupefacenti e innovative del pensiero
locale e globale.</p>

<p>“<b>Ideas worth spreading</b>”, idee che meritano di essere diffuse, è il motto e la mission di TED.
Le idee che circolano hanno la forza di cambiare il comportamento e la vita delle persone e il modo
in cui si relazionano l’una con l’altra, creando dei network che a loro volta generano altre idee.</p>
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-sm-7 about-box">
            <br /><br /><br /><br /><br />
            <h2>Far circolare le idee</h2>
            <br />
            <p>TED è un evento annuale che raggruppa grandi pensatori di tutto il mondo per condividere idee
che contano in diverse disciplina: tecnologia, divertimento, scienza, design, discipline umanistiche,
affari, sviluppo.</p>

<p>
L’ex presidente USA <b>Bill Clinton</b>, il fondatore di Microsoft <b>Bill Gates, James Dewey Watson</b>,
premio Nobel per la scoperta della struttura del DNA. E poi <b>Arianna Huffington</b>, fondatrice
dell’Huffington Post, <b>J.K. Rowling</b>, autrice della saga di Harry Potter, il regista <b>J.J. Abrams</b>,
l’artista <b>Marina Abramović</b>: questi sono solo alcuni dei nomi degli speaker che hanno dato il loro
contributo alle conferenze TED.</p>
        </div>
        <div class="col-sm-5">
            <img src="images/gate.png" width="90%" />
        </div>
    </div>
    <br /><br />
    <div class="row">
        <div class="col-sm-6">
            <img src="images/tedx.png" width="100%" />
        </div>
        <div class="col-sm-6 about-box">
            <h1>Il TEDx</h1>
            <h2>Idee globali, idee locali</h2>
            <br />
            <p>TED ha dato la possibilità di realizzare eventi locali, i <b>TEDx</b>, con l’obiettivo di trasmettere lo spirito
e l’esperienza TED a un pubblico sempre più vasto.</p>
<p>
TEDx è un forum di idee e di incontro costruito intorno a una comunità e a un territorio interessati
alla valorizzazione della scienza e della vita dell’uomo.
Ecco alcuni degli speaker che negli anni hanno partecipato agli eventi TEDx: la giornalista e
scrittrice <b>Mimosa Martini</b>, il divulgatore informatico <b>Salvatore Aranzulla</b>, lo scrittore <b>Alessandro
D&#39;Avenia</b>, lo stilista e imprenditore <b>Brunello Cucinelli</b>, l&#39;autore <b>Giulio Rapetti Mogol</b>, l&#39;attore e
scrittore <b>Marco Paolini</b>, l&#39;economista <b>Tito Boeri</b>.</p>
        </div>
    </div>
    <br /><br />
    
    <div class="row" style="text-align: center;">
        <div class="col-sm-12"><br /><br /><br /><br />
            <img src="images/world.png" width="70%" />
        </div>
        <div class="col-sm-12"><br /><br /><br /><br />
            <img src="images/italy.png" width="70%" />
        </div>
    </div>
    <br /><br />

</div>