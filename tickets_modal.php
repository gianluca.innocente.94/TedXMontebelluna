<div class="modal modal-wide fade" id="tickets_modal" style="margin-top: 200px;">
    <div class="modal-dialog">
        <div class="modal-content" style="background: #000000">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" style="color: #FFFFFF; opacity: 1 !important;">&times;</span>
                </button>
                <h4 class="modal-title">Acquista il biglietto</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="sponsor_form">
                    <div class="modal-body">
                        I biglietti saranno disponibili dal 20 aprile alle ore 10:00.
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>