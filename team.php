

<!--====  End of Page Title  ====-->

<section class="section contact-form no-bottom-line">
	<style>
footer {
    border-top: none;
}

.team-popup-info p {
    line-height: 30px;
}

.team-popup-container {
    padding-top: 0px;
    top: 30px;
}

.no-padding {
	margin-bottom: 30px;
}

</style>
<div class="row" style="position: relative; margin: 0;">
    
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ANDREA"><img src="assets/images/team/black/ANDREA.jpg" width="100%" id="image_ANDREA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ANDREA">
                    <div class="team-fast-hover">
                        <h1>Andrea Franchin</h1>
                        <h2>Licenziatario e Organizzatore</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ANDREA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_EMILIANO"><img src="assets/images/team/black/EMILIANO.jpg" width="100%" id="image_EMILIANO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_EMILIANO">
                    <div class="team-fast-hover">
                        <h1>Emiliano Guerra</h1>
                        <h2>Rapporti con la Pubblica Amministrazione & Team partner</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="EMILIANO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FEDERICA"><img src="assets/images/team/black/FEDERICA.jpg" width="100%" id="image_FEDERICA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FEDERICA">
                    <div class="team-fast-hover">
                        <h1>Federica Favero</h1>
                        <h2>Responsabile Team Produzione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FEDERICA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_BEATRICE"><img src="assets/images/team/black/BEATRICE.jpg" width="100%" id="image_BEATRICE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_BEATRICE">
                    <div class="team-fast-hover">
                        <h1>Beatrice Vecchiato</h1>
                        <h2>Coordinatore Traduzioni</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="BEATRICE">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ALBERTO"><img src="assets/images/team/black/ALBERTO.jpg" width="100%" id="image_ALBERTO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ALBERTO">
                    <div class="team-fast-hover">
                        <h1>Alberto Moretto</h1>
                        <h2>Gestione Vendite & Comunicazione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ALBERTO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_GIANLUCA"><img src="assets/images/team/black/GIANLUCA.jpg" width="100%" id="image_GIANLUCA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_GIANLUCA">
                    <div class="team-fast-hover">
                        <h1>Gianluca Innocente</h1>
                        <h2>Sviluppatore Web</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="GIANLUCA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_SERENA"><img src="assets/images/team/black/SERENA.jpg" width="100%" id="image_SERENA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_SERENA">
                    <div class="team-fast-hover">
                        <h1>Francesco Serena</h1>
                        <h2>Social Media Manager</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="SERENA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ARIANNA"><img src="assets/images/team/black/ARIANNA.jpg" width="100%" id="image_ARIANNA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ARIANNA">
                    <div class="team-fast-hover">
                        <h1>Arianna Ceschin</h1>
                        <h2>Copy & Ufficio Stampa</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ARIANNA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FRANCESCO"><img src="assets/images/team/black/FRANCESCO.jpg" width="100%" id="image_FRANCESCO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FRANCESCO">
                    <div class="team-fast-hover">
                        <h1>Francesco Calzamatta</h1>
                        <h2>Art Director</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FRANCESCO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_GIULI"><img src="assets/images/team/black/GIULI.jpg" width="100%" id="image_GIULI" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_GIULI">
                    <div class="team-fast-hover">
                        <h1>Giulia Bizzotto</h1>
                        <h2>Coordinatore Partner</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="GIULI">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_ENRICO"><img src="assets/images/team/black/ENRICO.jpg" width="100%" id="image_ENRICO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_ENRICO">
                    <div class="team-fast-hover">
                        <h1>Enrico Poloni</h1>
                        <h2>Team Partner</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="ENRICO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FRANCESCA"><img src="assets/images/team/black/FRANCESCA.jpg" width="100%" id="image_FRANCESCA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FRANCESCA">
                    <div class="team-fast-hover">
                        <h1>Francesca Celato</h1>
                        <h2>Coordinatore Speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FRANCESCA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_GIULIA"><img src="assets/images/team/black/GIULIA.jpg" width="100%" id="image_GIULIA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_GIULIA">
                    <div class="team-fast-hover">
                        <h1>Giulia Michielin</h1>
                        <h2>Coordinatore Speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="GIULIA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_LARA"><img src="assets/images/team/black/LARA.jpg" width="100%" id="image_LARA" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_LARA">
                    <div class="team-fast-hover">
                        <h1>Lara Citarei</h1>
                        <h2>Coordinatore Speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="LARA">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FABIO"><img src="assets/images/team/black/FABIO.jpg" width="100%" id="image_FABIO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FABIO">
                    <div class="team-fast-hover">
                        <h1>Fabio Donà</h1>
                        <h2>Coordinatore Speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FABIO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_FOGALE"><img src="assets/images/team/black/FOGALE.jpg" width="100%" id="image_FOGALE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_FOGALE">
                    <div class="team-fast-hover">
                        <h1>Daniele Fogale</h1>
                        <h2>Coordinatore Speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="FOGALE">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_JACOPO"><img src="assets/images/team/black/JACOPO.jpg" width="100%" id="image_JACOPO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_JACOPO">
                    <div class="team-fast-hover">
                        <h1>Jacopo Pellizzari</h1>
                        <h2>Coordinatore Speaker</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="JACOPO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_CARRARO"><img src="assets/images/team/black/CARRARO.jpg" width="100%" id="image_CARRARO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_CARRARO">
                    <div class="team-fast-hover">
                        <h1>Enrico Carraro</h1>
                        <h2>Team Produzione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="CARRARO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_CELATO"><img src="assets/images/team/black/CELATO.jpg" width="100%" id="image_CELATO" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_CELATO">
                    <div class="team-fast-hover">
                        <h1>Alberto Celato</h1>
                        <h2>Team Produzione</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="CELATO">

                </div>
            </div>
        </a>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 no-padding">
        <a href="javascript:void(0);" id="link_DANIELE"><img src="assets/images/team/black/DANIELE.jpg" width="100%" id="image_DANIELE" />
            <div class="team-fast-hover-container">
                <div class="team-fast-hover-background" id="hover_content_DANIELE">
                    <div class="team-fast-hover">
                        <h1>Daniele Bozzano</h1>
                        <h2>Fotografo</h2>
                    </div>
                </div>
                <div class="team-fast-hover-control team-hover" id="DANIELE">

                </div>
            </div>
        </a>
    </div>

    <div class="red-back"></div>
</div>
</section>
<script>
$(document).ready(function() {
        $(".team-hover").hover(function() {
            var element = $("#hover_content_" + this.id);
            var display = element.css("display");

            console.log("display "+display+"  for id "+this.id);

            if(display == 'none') {
                $("#image_"+this.id).attr("src", "assets/images/team/colors/" + this.id + ".jpg");
                element.show();
                console.log("show "+this.id);
            }
        }, function() {
            var display = this.style.display;

            console.log("hide "+display+"  id "+this.id);

            $("#image_"+this.id).attr("src", "assets/images/team/black/" + this.id + ".jpg");
            $("#hover_content_" + this.id).hide();
        });
});
</script>