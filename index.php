<?php
require("core/seo.php");

if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "home";
}

$allowed_pages = array("home", "contatti", "diventa-partner", "about", "speakers", "team", "partner", "talk", "gallery");

if(!in_array($page, $allowed_pages)) {
    $page = "home";
}
?>
<!DOCTYPE html>
<html lang="it">
<head>

  <!-- SITE TITTLE -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $titles[$page]; ?></title>
  <meta name="description" content="<?php echo $descriptions[$page]; ?>" />
    <meta name="keywords" content="tedx montebelluna, ted montebelluna" />
    <meta property="og:title" content="TEDxMontebelluna, evento TED organizzato in modo indipendente." />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="http://tedxmontebelluna.com/assets/images/social.jpg" />
    <meta property="og:url" content="http://www.tedxmontebelluna.com" />
    <meta property="og:locale" content="it_IT" />
  
  <!-- PLUGINS CSS STYLE -->
  <!-- Bootstrap -->
  <link href="assets/theme/bootstrap.min.css" rel="stylesheet">
  <!-- Font Awesome -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/fonts/stylesheet.css" rel="stylesheet">
  <!-- Magnific Popup -->
  <link href="assets/theme/magnific-popup.css" rel="stylesheet">
  <!-- Slick Carousel -->
  <link href="assets/theme/slick.css" rel="stylesheet">
  <link href="assets/theme/slick-theme.css" rel="stylesheet">
  <!-- CUSTOM CSS -->
  <link href="assets/theme/style.css?2" rel="stylesheet">
  <link href="assets/css/custom.css?2" rel="stylesheet">

 

    <link rel="apple-touch-icon" sizes="57x57" href="assets/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="assets/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="assets/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ff2b06">
    <meta name="msapplication-TileImage" content="assets/images/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ff2b06">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body class="body-wrapper">


<!--========================================
=            Navigation Section            =
=========================================-->

<nav class="navbar main-nav border-less fixed-top navbar-expand-lg p-0">
  <div class="container-fluid p-0">
      <!-- logo -->
      <a class="navbar-brand" href="home">
        <img src="assets/images/logo_white.png" alt="logo" height="60">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fa fa-bars"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav mx-auto">
		<li class="nav-item <?php if($page == "home") echo "active"; ?>">
          <a class="nav-link" href="home">Home<span>/</span></a>
        </li>
        <li class="nav-item <?php if($page == "speakers") echo "active"; ?>">
          <a class="nav-link" href="speakers">Speakers
            <span>/</span>
          </a>
        </li>
        <li class="nav-item <?php if($page == "team") echo "active"; ?>">
          <a class="nav-link" href="team">Team<span>/</span></a>
        </li>
        <li class="nav-item <?php if($page == "partner") echo "active"; ?>">
          <a class="nav-link" href="partner">Partner<span>/</span></a>
        </li>
        <li class="nav-item <?php if($page == "about") echo "active"; ?>">
          <a class="nav-link" href="about">About TED<span>/</span></a>
        </li>
		<li class="nav-item <?php if($page == "contatti") echo "active"; ?>">
          <a class="nav-link" href="contatti">Contatti<span>/</span></a>
        </li>
		<li class="nav-item">
          <a class="nav-link" href="2018/" target="_blank">Edizione 2018</a>
        </li>
      </ul>
      <a href="gallery" class="ticket">
        <img src="assets/images/video.png" width="20" alt="Guarda la gallery">
        <span>GALLERY</span>
      </a>
      </div>
  </div>
</nav>

<!--====  End of Navigation Section  ====-->

<?php
include($page . ".php");
?>

<!--============================
=            Footer            =
=============================-->

<footer class="footer-main">
    <div class="container">
      <div class="row flex-column-reverse flex-md-row">
        <div class="col-md-4 block-info-footer">
          <div class="block">
            <p>Comitato Promotore TEDxMontebelluna<br />
      Piazza Oberkochen 25/4 - Montebelluna (TV) 31044<br />
      C.F. 92044330261 - P.IVA 04938770262<br />
      <a href="mailto:info@tedxmontebelluna.com">info@tedxmontebelluna.com</a></p>
          </div>
          
        </div>
        <div class="col-md-4">
          <div class="block text-center">
            <div class="footer-logo">
              <img src="assets/images/logo_white.png" alt="logo" class="img-fluid" width="170">
            </div>
          </div>
          
        </div>
        <div class="col-md-4 block-social-footer">
          <div class="block">
            <ul class="social-links-footer list-inline">
              <li class="list-inline-item">
                <a href="https://www.facebook.com/tedxmontebelluna/"><i class="fa fa-facebook"></i></a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.instagram.com/tedxmontebelluna/"><i class="fa fa-instagram"></i></a>
              </li>
            </ul>
          </div>
          
        </div>
      </div>
    </div>
    <div class="mobile-footer-logo">
        <img src="assets/images/logo_white.png" alt="logo" class="img-fluid" width="170">
      </div>
</footer>
<!-- Subfooter -->
<!--<footer class="subfooter">
  <div class="container">
    <div class="row">
      <div class="col-md-6 align-self-center">
        <div class="copyright-text">
          <p><a href="#">TEDxMontebelluna</a> &#169; 2019 All Right Reserved</p>
        </div>
      </div>
      <div class="col-md-6">
          <a href="#" class="to-top"><i class="fa fa-angle-up"></i></a>
      </div>
    </div>
  </div>
</footer>-->

<?php
include("tickets_modal.php");
?>


  <!-- JAVASCRIPTS -->
  <!-- jQuey -->
  <script src="assets/theme/jquery.js"></script>
  <!-- Popper js -->
  <script src="assets/theme/popper.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="assets/theme/bootstrap.min.js"></script>
  <!-- Smooth Scroll -->
  <script src="assets/theme/SmoothScroll.min.js"></script>  
  <!-- Isotope -->
  <script src="assets/theme/mixitup.min.js"></script>  
  <!-- Magnific Popup -->
  <script src="assets/theme/jquery.magnific-popup.min.js"></script>
  <!-- Slick Carousel -->
  <script src="assets/theme/slick.min.js"></script>  
  <!-- SyoTimer -->
  <script src="assets/theme/jquery.syotimer.min.js"></script>
  
  <!-- Custom Script -->
  <script src="assets/theme/custom.js"></script>

  <script>
  function showModal(modalID) {
    $('#' + modalID).modal({
        show: true,
        backdrop: 'static'
    });
}

function hideModal(modalID) {
    $('#' + modalID).modal("hide");
}
</script>
</body>

</html>