<?php
$titles = [
    "home" => "Home - TEDxMontebelluna",
    "contatti" => "Contatti - TEDxMontebelluna",
    "diventa-partner" => "Diventa partner - TEDxMontebelluna",
    "partner" => "Partner- TEDxMontebelluna",
    "about" => "About - TEDxMontebelluna",
    "speakers" => "Speakers - TEDxMontebelluna",
    "team" => "Team - TEDxMontebelluna",
    "talk" => "Le Talk - TEDxMontebelluna",
    "gallery" => "Gallery - TEDxMontebelluna"
];

$descriptions = [
    "home" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "contatti" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "diventa-partner" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "partner" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "about" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "speakers" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "team" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "talk" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna",
    "gallery" => "TEDxMontebelluna, evento TED organizzato in modo indipendente. #TEDxMontebelluna"
];
?>
