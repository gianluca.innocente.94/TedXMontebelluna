<?php
require(dirname(dirname(__FILE__))."/libs/phpmailer/PHPMailerAutoload.php");

if(isset($_POST['name'], $_POST['captcha_response'], $_POST['email'], $_POST['notes'], $_POST['phone'])) {
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $captcha_response = $_POST['captcha_response'];
    $email = $_POST['email'];
    $notes = $_POST['notes'];

    if($name != "" && $captcha_response != "" && $email != "" && $phone != "") {
        $data = array(
            'secret' => "6LfWq1EUAAAAAL1FpZrtVpq0htMrG6UVuCYInTuQ",
            'response' => $captcha_response
        );

        $verify = curl_init();
        curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($verify, CURLOPT_POST, true);
        curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($verify);

        //var_dump($response);

        if(@$response["success"] == true) {
            $subject = "Nuova richiesta di partnership su TEDxMontebelluna!";
            $msg = "Nuova richiesta di partnership su TEDxMontebelluna!
            <br /><br />
            Nome: ".$name."<br />
            Email: ".$email."<br />
            Telefono: ".$phone."<br />
            Note: ".$notes."<br />";



            $mail = new PHPMailer;

            //$mail->SMTPDebug = 3;                               // Enable verbose debug output

            $mail->CharSet = 'UTF-8';
            $mail->isHTML(true);// TCP port to connect to
            $mail->Sender = "info@tedxmontebelluna.com";

            $mail->ClearAllRecipients();
            $mail->From = "info@tedxmontebelluna.com";
            $mail->FromName = 'TEDxMontebelluna';
            $mail->addAddress("partner@tedxmontebelluna.com");
            $mail->AddReplyTo($email, $name);
            $mail->Subject = $subject;
            $mail->Body    = $msg;

            $second = $mail->send();

            if($second) {
                echo "La tua richiesta è stata inviata al nostro team. Sarai ricontattato a breve. Grazie!";
            } else {
                echo "Si è verificato un errore imprevisto. Ti preghiamo di contattarci scrivendo a info@tedxmontebelluna.com";
            }
        } else {
            echo "Errore: captcha non verificato";
        }
    } else {
        echo "Errore: tutti i campi sono obbligatori";
    }
} else {
    echo "Errore. Tutti i campi sono obbligatori";
}
?>
